estacaoApp.cC({
	name: 'mapasCtrl',
	inject: ['$scope', '$resource','dataService', '$sanitize'],
	data: {
		DS: 'dataService',
		map: {
			center: {
				latitude: -14,
				longitude: -25
			},
			zoom: 4, 
			options: {
				draggable: 1,
				panControl: 0,
				mapTypeControl: 0
			}
		},
		map: {
			center: {
				latitude: -14,
				longitude: -25
			},
			zoom: 4, 
			options: {
				draggable: 1,
				panControl: 0,
				mapTypeControl: 0
			}
		},
		markers: {
			estados: [],
			municipios: []
		}
	},
	init: function() {
		this.getEstados();
		this.getMunicipios();
		this.getOcorrencias();
	},
	watch: {
		'{collection}DS.camadas': 'changeCamada',
		'{collection}DS.selecteds': 'changeVisao',
		'{collection}DS.flags':'changeFlags'
	},
	methods: {
		// Altera Camada
		changeCamada: function(newv, oldv) {
			this.DS.flags.showLocalizacaoCtrl = false;
			this.$.camadas = newv;
		},
		// Altera Visa
		changeVisao: function(newv, oldv) {
			this.DS.flags.showLocalizacaoCtrl = false;
			this.$.selecteds = newv;
		},
		changeFlags: function(newv, oldv) {
			this.$.flags = newv;
		},
		// Estados
		getEstados: function() {
			var vm = this;
			this.$resource('/mapas/ajax_estados').query().$promise
				.then(function(result) {
					vm.$.estados = result;
					vm.setEstadosMarkers();
			});
		},
		setEstadosMarkers: function() {
			var vm = this;
			var estados = [];
			vm.$.estados.forEach(function(estado){
				
				estados.push(
					{
						id: estado.Estado.id,
						key: 'E'+estado.Estado.id,
						options: {
							labelContent: estado.Estado.nome,
							labelClass: 'label label-info',
							labelAnchor: '15 0'
						},
						coords: [estado.Estado.longitude, estado.Estado.latitude],
						zoom: estado.Estado.zoom,
						icon: '/img/markers/localizacao.png',
						data: estado
					}
				);
			});
			vm.$.markers.estados = estados;
		},
		clickEstadoMarker: function(item, event, options) {
			this.DS.selecteds.Estado = options.data;
			this.getMunicipios();
			this.getOcorrencias();
			this.DS.camadas.estados = false;
			this.DS.camadas.municipios = true;
		},
		changeEstadoCombo: function() {
			this.DS.selecteds.Estado = this.DS.selecteds.Estado;
			this.getMunicipios();
			this.getOcorrencias();
			this.DS.camadas.estados = false;
			this.DS.camadas.municipios = true;
		},
		
		// Municipios
		getMunicipios: function() {
			var vm = this;
			if (vm.DS.selecteds.Estado == undefined) {
				url = '/mapas/ajax_municipios';
			} else {
				url = '/mapas/ajax_municipios/'+vm.DS.selecteds.Estado.Estado.id;
			}
			this.$resource(url).query().$promise
				.then(function(result) {
					vm.$.municipios = result;
					vm.setMunicipiosMarkers();
			});
		},
		setMunicipiosMarkers: function() {
			var vm = this;
			var municipios = [];
			vm.$.municipios.forEach(function(municipio){
				
					municipios.push(
					{
						id: municipio.Municipio.id,
						key: 'M'+municipio.Municipio.id,
						options: {
							labelContent: municipio.Municipio.nome,
							labelClass: 'label label-info',
							labelAnchor: '15 0'
						},
						coords: [municipio.Municipio.longitude, municipio.Municipio.latitude],
						zoom: municipio.Municipio.zoom,
						icon: '/img/markers/logo_ej_marker.png',
						data: municipio
					}					
				);
				
			});
			vm.$.markers.municipios = municipios;
		},
		clickMunicipioMarker: function(item, event, options) {
			this.DS.selecteds.Municipio = options.data;
			this.getOcorrencias();
			this.DS.camadas.municipios = false;
			this.DS.camadas.ocorrencias = true;
		},
		changeMunicipioCombo: function() {
			this.DS.selecteds.Municipio = this.DS.selecteds.Municipio;
			this.getOcorrencias();
			this.DS.camadas.municipios = false;
			this.DS.camadas.ocorrencias = true;
		},
		
		// Ocorrencias
		getOcorrencias: function() {
			var vm = this;
			if (vm.DS.selecteds.Municipio == undefined) {
				url = '/mapas/ajax_ocorrencias';
			} else {
				url = '/mapas/ajax_ocorrencias/'+vm.DS.selecteds.Municipio.Municipio.id;
			}
			this.$resource(url).query().$promise
				.then(function(result) {
					vm.$.ocorrencias = result;
					vm.setOcorrenciasMarkers();
			});
		},
		setOcorrenciasMarkers: function() {
			var vm = this;
			var localizacoes = [];
			vm.$.ocorrencias.forEach(function(ocorrencia){
				ocorrencia.Localizacao.forEach(function(local){
					localizacoes.push(
					{
						id: local.id,
						key: 'L'+local.id,
						options: {
							labelContent: ocorrencia.Programa.nome_divulgacao,
							labelClass: 'label label-info',
							labelAnchor: '15 0'
						},
						coords: [local.longitude, local.latitude],
						zoom: local.zoom,
						icon: '/img/markers/logo_ej_marker.png',
						data: ocorrencia
					}					
					);
				});
			});
			vm.$.markers.localizacoes = localizacoes;
		},
		clickLocalizacaoMarker: function(item, event, options) {
			var vm = this;
			vm.DS.selecteds.Localizacao = options;
			vm.DS.flags.showLocalizacaoCtrl = true;
			console.log(vm.DS.flags);
			
		},
	}
});