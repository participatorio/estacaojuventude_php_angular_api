estacaoApp.directive('compileTemplate', function($compile, $parse){
	return {
	    link: function(scope, element, attr){
	        var parsed = $parse(attr.ngBindHtml);
	        function getStringValue() { return (parsed(scope) || '').toString(); }
	
	        //Recompile if the template changes
	        scope.$watch(getStringValue, function() {
	            $compile(element, null, -9999)(scope);  
	            //The -9999 makes it skip directives so that we do not recompile ourselves
	        });
	    }
	}
});

estacaoApp.cC({
	name: 'pesquisasCtrl',
	inject: ['$scope', 'NgMap', '$resource','dataService', '$sanitize', '$sce','$timeout'],
	data: {
		DS: 'dataService',
		markers: {},
		tematicasHtml: '',
		nivel: 'N',
		camadas: {
			estados: true,
			municipios: false,
			ocorrencias: false,
			voce: false
		},
		mapSetup: {
			coords: [-14,-25],
			zoom: 4, 
			options: {
				draggable: 1,
				panControl: 0,
				mapTypeControl: 0,
				scrollwheel: false
			}
		},
		campo_pesquisa: '',
		selecionados: {
			voce: null,
			tematica: null,
			estado: null,
			municipio: null,
			ocorrencia: null,
			tematicas: []
		},
		infoOptions: {
			visible: true
		}
		
	},
	watch: {
		'{object}selecionados.estado':'selectEstado',
		'{object}selecionados.municipio':'selectMunicipio',
		'{object}selecionados.tematica':'addTematica'
	},
	init: function() {
		var vm = this;
		vm.getTematicas();
		vm.showTematicas();
		vm.NgMap.getMap().then(function(map){
			vm.map = map;
		});
		
	},
	methods: {
		addTematica: function(newv, oldv) {
			if (newv == oldv || newv == null) return false;
			var vm = this;
			vm.$.selecionados.tematicas.push(vm.$.selecionados.tematica);
			vm.showTematicas();
			vm.getTematicas();
		},
		rmTematica: function(tematica_id) {
			var vm = this;
			vm.$.selecionados.tematicas.every(function(item, index){
				
				if (tematica_id == item.Tematica.id) {
					vm.$.selecionados.tematicas.splice(index,1);
					return false;
				} else {
					return true;
				}
			});
			vm.showTematicas();
			vm.getTematicas();
		},
		showTematicas: function() {
			var vm = this;
			var html = '';
			
			if (vm.$.selecionados.tematicas.length > 0) {
				vm.$.selecionados.tematicas.forEach(function(item){
					html+= '<span ng-click="rmTematica('+item.Tematica.id+')" class="label label-info" style="cursor: pointer;" data-toggle="tooltip" title="Clique para remover">'+item.Tematica.nome+'</span>&nbsp;';
				});
			} else {
				html+= '<span class="label label-warning">Todas</span>';
			}
			vm.$.tematicasHtml = vm.$sce.trustAsHtml(html);
			vm.getEstados();
			vm.getMunicipios();
			vm.getOcorrencias();
		},
		geoLocalization: function() {
			var vm = this;
			navigator.geolocation.getCurrentPosition(vm.found);
		},
		found: function(pos) {
			var vm = this;
			vm.$.$apply(function () {
				vm.setVoceMarkers(pos);
			});
		},
		setVoceMarkers: function(pos) {
			var vm = this;
			var voce = [];
			vm.$.camadas.voce = true;
			vm.$.camadas.estados = false;
			vm.$.camadas.municipios = false;
			vm.$.camadas.ocorrencias = false;
			voce.push(
				{
					id: 1,
					key: 'V1',
					options: {
						labelContent: 'Você',
						labelClass: 'label label-danger',
						labelAnchor: '12 0'
					},
					coords: [pos.coords.latitude, pos.coords.longitude],
					zoom: 6,
					icon: '/img/markers/red_marker.png'
				}
			);

			vm.$.markers.voce = voce;
		},
		clickCamada: function(camada) {
			var vm = this;
			vm.$.camadas.voce = false;
			vm.$.camadas.estados = false;
			vm.$.camadas.municipios = false;
			vm.$.camadas.ocorrencias = false;
			
			if (camada == 'E') {
				vm.$.camadas.estados = true;
			}
			if (camada == 'M') {
				vm.$.camadas.municipios = true;
			}
			if (camada == 'O') {
				vm.$.camadas.ocorrencias = true;
			}
		},
		selectTematica: function() {
		},
		clickEstadoMarker: function(ini, marker) {
			var vm = this;
			vm.$.selecionados.estado = marker.data;
		},
		selectEstado: function(newv, oldv) {
			if (newv == oldv) return false;
			var vm = this;
			vm.$.getMunicipios();
			vm.$.getOcorrencias();
			vm.clickCamada('M');
		},
		clickMunicipioMarker: function(ini, marker) {
			var vm = this;
			vm.$.selecionados.municipio = marker.data;
		},
		selectMunicipio: function(newv, oldv) {
			if (newv == oldv) return false;
			var vm = this;
			vm.$.getOcorrencias();
			vm.clickCamada('O');
		},
		clickLocalizacaoMarker: function(ini, marker) {
			var vm = this;
			var data = marker.data;
			var inicio = new Date(data.Ocorrencia.inicio_inscricoes);
			var fim = new Date(data.Ocorrencia.fim_inscricoes);
			vm.$.selectedLocalizacao = marker;
			vm.$.infoHtml = {
				position: [data.Localizacao[0].latitude, data.Localizacao[0].longitude],
				programa: data.Programa.nome_oficial,
				de: inicio.getDate()+'/'+inicio.getMonth()+'/'+inicio.getFullYear(),
				ate: fim.getDate()+'/'+fim.getMonth()+'/'+fim.getFullYear(),
				descricao: vm.$sce.trustAsHtml(data.Programa.descricao)
			};
			vm.map.showInfoWindow('OcorrenciaInfo');
		},
		fechaOcorrenciaView: function() {
			var vm = this;
			vm.$.selecionados.ocorrencia = false;
			$('#viewOcorrencia').modal('hide');
		},
		closeInfo: function() {
			this.$.infoOptions.visible = false;
		},
		clickLocalizacaoVerMais: function() {
			var vm = this;
			vm.$.selecionados.ocorrencia = vm.$.selectedLocalizacao.data;
			$('#viewOcorrencia').modal('show');
		},
		clickOcorrenciaMarker: function(ini, ini2, marker) {
			var vm = this;
			vm.$.selecionados.ocorrencia = marker.data;
		},
		selectOcorrencia: function() {
		},
		setOcorrencias: function() {
			var vm = this;
			if (vm.camadas.ocorrencias)
				vm.camadas.ocorrencias = false;
			else
				vm.camadas.ocorrencias = true;
		},
		
		setEstadosMarkers: function() {
			var vm = this;
			var estados = [];
			vm.$.estados.forEach(function(estado){
					estados.push(
					{
						id: estado.Estado.id,
						key: 'E'+estado.Estado.id,
						labelClass: 'label label-danger',
						labelAnchor: '12 0',
						label: estado.Estado.sigla,
						coords: [ parseFloat(estado.Estado.latitude), parseFloat(estado.Estado.longitude)],
						zoom: estado.Estado.zoom,
						icon: '/img/markers/red_marker.png',
						data: estado
					}					
					);

			});
			vm.$.markers.estados = estados;
		},
		
		setMunicipiosMarkers: function() {
			var vm = this;
			var municipios = [];
			vm.$.municipios.forEach(function(municipio){
				
				municipios.push(
					{
						id: municipio.Municipio.id,
						key: 'M'+municipio.Municipio.id,
						options: {
							labelContent: municipio.Municipio.nome,
							labelClass: 'label label-primary',
							labelAnchor: '18 0'
						},
						coords: [ parseFloat(municipio.Municipio.latitude), parseFloat(municipio.Municipio.longitude)],
						zoom: municipio.Municipio.zoom,
						icon: '/img/markers/logo_ej_marker.png',
						data: municipio
					}					
				);

			});
			vm.$.markers.municipios = municipios;
		},
		
		setOcorrenciasMarkers: function() {
			var vm = this;
			var localizacoes = [];
			vm.$.ocorrencias.forEach(function(ocorrencia){
				ocorrencia.Localizacao.forEach(function(local){
					localizacoes.push(
					{
						id: local.id,
						key: 'L'+local.id,
						options: {
							labelContent: ocorrencia.Programa.nome_divulgacao,
							labelClass: 'label label-info',
							labelAnchor: '15 0'
						},
						coords: [parseFloat(local.latitude), parseFloat(local.longitude)],
						zoom: local.zoom,
						icon: '/img/markers/red_flag.png',
						data: ocorrencia
					}					
					);
				});
			});
			vm.$.markers.localizacoes = localizacoes;
		},
		
		getTematicas: function() {
			var vm = this;
			this.$resource('/mapas/ajax_tematicas').query({exctem:vm.excTemIds()}).$promise
				.then(function(result){
				vm.dataService.tematicas = result;
				vm.$.tematicas = vm.dataService.tematicas;
			});
		},
		excTemIds: function() {
			var vm = this;
			var ids = [];
			vm.$.selecionados.tematicas.forEach(function(item){
				ids.push(item.Tematica.id);
			});
			return ids.toString();
		},
		getEstados: function() {
			var vm = this;
			this.$resource('/mapas/ajax_estados').query({tematicas:vm.excTemIds()}).$promise
				.then(function(result){
				vm.dataService.estados = result;
				vm.$.estados = vm.dataService.estados;
				vm.setEstadosMarkers();
			});
		},
		getMunicipios: function() {
			var vm = this;
			if (vm.$.selecionados.estado) {
				url = '/mapas/ajax_municipios/' + vm.$.selecionados.estado.Estado.id;
			} else {
				url = '/mapas/ajax_municipios/';
			}
			vm.$.municipios = [];
			this.$resource(url).query({tematicas:vm.excTemIds()}).$promise
				.then(function(result){
				vm.dataService.municipios = result;
				vm.$.municipios = vm.dataService.municipios;
				vm.setMunicipiosMarkers();
			});
		},
		pesquisaOcorrencias: function() {
			var vm = this;
			vm.getOcorrencias();
			vm.clickCamada('O');
		},
		getOcorrencias: function() {
			var vm = this;
			url = '/mapas/ajax_ocorrencias';
			if (vm.$.selecionados.municipio) {
				url+= '/' + vm.$.selecionados.municipio.Municipio.id;
			} else {
				url+= '/0'
			}
			if (vm.$.selecionados.estado) {
				url+= '/' + vm.$.selecionados.estado.Estado.id;
			}
			vm.$.ocorrencias = [];
			this.$resource(url).query({
					tematicas:vm.excTemIds(),
					pesquisa: vm.$.campo_pesquisa,
				}).$promise
				.then(function(result){
				vm.dataService.ocorrencias = result;
				vm.$.ocorrencias = vm.dataService.ocorrencias;
				vm.setOcorrenciasMarkers();
			});
		}
	}
});
