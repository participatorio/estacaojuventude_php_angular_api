estacaoApp.cC({
	name: 'tematicasCtrl',
	inject: ['$scope', '$resource','dataService'],
	data: {
		DS: 'dataService'
	},
	init: function() {
		
		if (this.dataService.tematicasCtrl == false) {
			this.dataService.tematicasCtrl = true;
			this.getTematicas();
		}

	},
	methods: {
		getTematicas: function() {
			var vm = this;
			this.$resource('/mapas/ajax_tematicas').query().$promise
				.then(function(result){
				vm.dataService.tematicas = result;
				vm.$.tematicas = vm.dataService.tematicas;
			});
		},
		isSelected: function(item) {
			var id = item.Tematica.id;
			var index = this.dataService.tematicas_ids.indexOf(id);
			if (index > -1)
				return true;
			else
				return false;
		},
		clickTematica: function(item) {
			var id = item.Tematica.id;
			var index = this.dataService.tematicas_ids.indexOf(id);
			if (index == -1) {
				this.dataService.tematicas_ids.push(item.Tematica.id);
			} else {
				this.dataService.tematicas_ids.splice(index,1);
			}
		},
		toggleCamada: function(camada) {
			var vm = this;
			vm.DS.camadas.estados = false;
			vm.DS.camadas.municipios = false;
			vm.DS.camadas.ocorrencias = false;
			
			vm.DS.camadas[camada] = true;
			
		},
		hideLocalizacaoCtrl: function() {
			var vm = this;
			vm.DS.flags.showLocalizacaoCtrl = false;
		}
	}
});