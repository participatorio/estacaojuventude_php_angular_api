estacaoApp.service('dataService', function() {
	return {
		flags: {
			showLocalizacaoCtrl: false,
		},
		tematicasCtrl: false,
		
		tematicas: [],
		tematicas_ids: [],
		selecteds: {
			tematicas_ids: []
		},
		visoes: {
			nacional: true,
			estadual: false,
			municipal: false,
			localizado: false,
		},
		camadas: {
			estados: true,
			municipios: false,
			ocorrencias: false,
			pesquisas: false
		}
	}
});