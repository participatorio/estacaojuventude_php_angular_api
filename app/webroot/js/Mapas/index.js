estacaoApp.cC({
	name: 'pesquisasCtrl',
	inject: ['$scope', '$resource','dataService', '$sanitize'],
	data: {
		DS: 'dataService',
		selectedItems: {
			Estado: null,
			Municipio: null,
			Ocorrencia: null,
			Programa: null,
			Localizacao: null
		},
		markers: {
			estados: [],
			municipios: [],
			localizacoes: []
		},
		camadas: {
			estados: true,
			estacoes: true,
			localizacoes: true
		},
		map: {
			center: {
				latitude: -14,
				longitude: -25
			},
			zoom: 4, 
			options: {
				draggable: 1,
				panControl: 0,
				mapTypeControl: 0
			}
		},
		centers: {
			estado: {},
			municipio: {},
			localizacao: {}
		}
	},
	init: function() {
		this.getEstados();
		this.$.categoria = 'textual';
		this.getTematicas();
	},
	methods: {
		visoes: function(tipo) {
			if (tipo == 'nacional') this._nacional();
			if (tipo == 'estadual') this._estadual();
			if (tipo == 'municipal') this._municipal();
			if (tipo == 'localizado') this._localizado();
		},
		_nacional: function() {
			this.$.map = { center: { latitude: -14, longitude: -25 }, zoom: 4};
		},
		_estadual: function() {
			var center = this.data.centers.estado;
			this.$.map = { center: { latitude: center.coords[1], longitude: center.coords[0] }, zoom: center.zoom};
		},
		_municipal: function() {
			var center = this.data.centers.estado;
			this.$.map = { center: { latitude: center.coords[1], longitude: center.coords[0] }, zoom: center.zoom};
		},
		_localizado: function() {
			this.$.map = { center: { latitude: -15.802237640861577, longitude: -47.853547334671006 }, zoom: 18};
		},
		
		toggleCamadaEstados: function() {
			this.camadas.estados = !this.camadas.estados;
		},
		toggleCamadaEstacoes: function() {
			this.camadas.estacoes =!this.camadas.estacoes;
		},
		toggleCamadaLocalizacoes: function() {
			this.camadas.localizacoes = !this.camadas.localizacoes;
		},
		
		changeEstado: function(item) {
			this.getMunicipios(item);
		},
		changeMunicipio: function(item) {
			this.getOcorrencias(item);
		},
		// Quando se escolhe um estado no modo Textual
		clickEstadoList: function(item) {
			if (item == 0) {
				this.$.selectedItems.Estado = {id:0, nome:'Todos'};
			} else {
				this.$.selectedItems.Estado = item;
			}
			
			// zera ocorrencias
			this.$.ocorrencias = [];
			// zero localizacoes
			
			this.$.localizacoes = [];
			
			// Delete objeto Ocorrencia
			delete(this.$.Ocorrencia);
			
			// Delete objeto Programa
			delete(this.$.Programa);
			
			// Carrega Municipios
			this.getMunicipios();
			
			// Emula o click no "label" do collapse Municipio
			$('#collapseTwo').prev().find('a').click();
		},
		// Quanto se escolhe um estado no modo Mapa
		clickEstadoMarker: function(mod, e, obj) {
			this.$.selectedItems.Estado = obj
			this.data.centers.estado = {
				coords: obj.coords,
				zoom: obj.zoom
			};
			
			// Altera para a visão estadual
			this._estadual();
			
			// Carrega Municipios
			this.getMunicipios();
		},
		clickMunicipioList: function(item) {
			if (item == 0) {
				this.$.selectedItems.Municipio = {id:0, nome:'Todos'};
			} else {
				this.$.selectedItems.Municipio = item;
			}
			
			this.$.localizacoes = [];
			delete(this.$.Ocorrencia);
			delete(this.$.Programa);
			this.getOcorrencias();
			$('#collapseThree').prev().find('a').click();
		},
		// Clique no marcador de Municipio
		clickMunicipioMarker: function(mod, e, obj) {
			this.$.municipio_id = obj.id;
			this.data.centers.municipio = {
				coords: obj.coords,
				zoom: obj.zoom
			};
			this._municipal();
			this.getOcorrencias();
		},
		clickOcorrenciaList: function(item) {
			this.$.selectedItems.Ocorrencia = item;
			
			this.$.Localizacao = [];
			this.$.localizacoes = item.Localizacao;
			this.$.selectedItems.Ocorrencia = item.Ocorrencia;
			this.$.selectedItems.Programa = item.Programa;
			this.$.selectedItems.Municipio = item.Municipio;
		},
		clickLocalizacaoList: function(item) {
			this.$.localizacao_id = item.id;
			this.$.Localizacao = item;
		},
		getTematicas: function() {
			var vm = this;
			this.$resource('/pesquisas/ajax_tematicas').query().$promise
				.then(function(result) {
					vm.$.tematicas = result;
			});
		},
		getEstados: function() {
			var vm = this;
			this.$resource('/pesquisas/ajax_estados').query().$promise
				.then(function(result) {
					vm.$.estados = result;
					vm.setEstadosMarkers();
			});
		},
		getMunicipios: function() {
			var vm = this;
			console.log(vm.estado);
			this.$resource('/pesquisas/ajax_municipios/'+vm.$.estado.Estado.id).query().$promise
				.then(function(result) {
					vm.$.municipios = result;
					vm.setMunicipiosMarkers();
			});
		},
		
		getOcorrencias: function() {
			var vm = this;
			//if (!vm.$.municipio_id) return false;
			this.$resource('/pesquisas/ajax_ocorrencias/'+vm.$.municipio_id).query().$promise
			.then(function(result) {
				vm.$.ocorrencias = result;
				vm.setOcorrenciasMarkers();
			});
		},
		setEstadosMarkers: function() {
			var vm = this;
			var estados = [];
			vm.$.estados.forEach(function(estado){
				
				estados.push(
					{
						id: estado.Estado.id,
						key: 'L'+estado.Estado.id,
						options: {
							labelContent: estado.Estado.nome,
							labelClass: 'label label-info',
							labelAnchor: '15 0'
						},
						coords: [estado.Estado.longitude, estado.Estado.latitude],
						zoom: estado.Estado.zoom,
						icon: '/img/markers/localizacao.png'
					}
				);
			});
			vm.$.markers.estados = estados;
		},
		setMunicipiosMarkers: function() {
			var vm = this;
			var municipios = [];
			vm.$.municipios.forEach(function(municipio){
				
				municipios.push(
					{
						id: municipio.Municipio.id,
						key: 'L'+municipio.Municipio.id,
						options: {
							labelContent: municipio.Municipio.nome,
							labelClass: 'label label-warning',
							labelAnchor: '15 0'
						},
						coords: [municipio.Municipio.longitude, municipio.Municipio.latitude],
						icon: '/img/markers/logo_ej_marker.png'
					}
					
				);
				
			});
			vm.$.markers.municipios = municipios;
			
		},
		setOcorrenciasMarkers: function() {
			var vm = this;
			var localizacoes = [];
			vm.$.ocorrencias.forEach(function(ocorrencia){
				ocorrencia.Localizacao.forEach(function(local){
					localizacoes.push(
						{ 
							id: local.id,
							key: 'L'+local.id,
							options: {
								labelContent: ocorrencia.Programa.nome_divulgacao,
								labelClass: 'label label-danger',
								labelAnchor: '15 0'
							},
							winContent: ocorrencia,
							coords: [local.longitude, local.latitude],
							icon: '/img/markers/localizacao.png'
						}
					);
				});
			});
			vm.$.markers.localizacoes = localizacoes;
		}
	}
});