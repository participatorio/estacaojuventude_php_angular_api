<?php
class Municipio extends AppModel {
	
	public $useTable = 'vw_municipios';
	
	public $belongsTo = array(
		'Estado'
	);
	
}