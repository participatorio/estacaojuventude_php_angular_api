<?php
class Orgao extends AppModel {
	
	public $useTable = 'orgaos';
	
	public $order = [
		'Orgao.nome' => 'ASC'
	];
	
	public $belongsTo = array(
		'Municipio' => array(
			'className' => 'Admin.Municipio'
		),
		'Estado' => array(
			'className' => 'Admin.Estado'
		)
	);
	
	public $hasMany = [
		'OrgaoPrograma' => [
			'className' => 'Admin.OrgaoPrograma',
			'foreingKey' => 'orgao_id'
		],
		'OcorrenciaOrgao' => [
			'className' => 'Admin.OcorrenciaOrgao',
			'foreingKey' => 'orgao_id'
		]
	];

	
}
