<?php
class Programa extends AppModel {
	
	public $useTable = 'programas';
	
	public $hasMany = array(
		'OrgaoPrograma' => array(
			'className' => 'OrgaoPrograma',
			'foreignKey' => 'programa_id'
		)
	);
	
	public $hasAndBelongsToMany = [
		'TematicaAll' => [
			'joinTable' => 'programas_tematicas',
			'foreignKey' => 'programa_id',
			'associationForeignKey' => 'tematica_id'
		]
		
	];
	
	public $order = ['Programa.nome_oficial'=>'ASC'];

}