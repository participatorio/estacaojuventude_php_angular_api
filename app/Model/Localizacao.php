<?php
class Localizacao extends AppModel {
	
	public $useTable = 'localizacoes';
	
	public $belongsTo = array(
		'Municipio'
	);
	
}