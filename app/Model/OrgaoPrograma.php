<?php
class OrgaoPrograma extends AppModel {
	
	public $useTable = 'orgaos_programas';
	
	public $belongsTo = [
		'Orgao',
		'Programa'	
	];

	
}
