<?php
class Ocorrencia extends AppModel {
	
	public $useTable = 'vw_ocorrencias';
	
	public $hasMany = array(
		'Localizacao' => array(
			'className' => 'Localizacao',
			'foreignKey' => 'ocorrencia_id'
		),
		'OcorrenciaOrgao' => array(
			'className' => 'OcorrenciaOrgao',
			'foreignKey' => 'ocorrencia_id'
		)
	);
	
	public $belongsTo = array(
		'Programa',
		'Municipio'
	);
}