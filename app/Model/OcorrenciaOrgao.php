<?php
class OcorrenciaOrgao extends AppModel {
	
	public $useTable = 'ocorrencias_orgaos';
	
	public $belongsTo = [
		'Orgao',
		'Ocorrencia'
	];
}
