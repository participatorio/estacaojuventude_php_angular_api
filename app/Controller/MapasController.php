<?php
	class MapasController extends AppController {
		
		public $uses = ['Tematica','Estado','Municipio','Ocorrencia'];
		
		public function index() {
			
		}
		
		public function load_tematicas() {
			$exclude_ids = $this->request->query['exctem'];
			if (!$exclude_ids) $exclude_ids = '0';
			$conditions = array(
				'NOT' => array(
					'id' => explode(',', $exclude_ids)
				)
			);
			$tematicas = $this->Tematica->find(
				'all',
				array(
					'order' => array(
						'Tematica.nome' => 'asc'
					),
					'conditions' => $conditions
				)
			);
			return $tematicas;
		}
		
		public function ajax_tematicas($exclude_ids = null) {
		
			$this->layout = 'ajax';
			
			$tematicas = $this->load_tematicas($exclude_ids);
			
			echo json_encode($tematicas);
	
			$this->render(false);
		}
		
		public function ajax_estados() {
			$tematicas_ids = $this->request->query['tematicas'];
			if (!$tematicas_ids) $tematicas_ids = '0';
			
			$this->layout = 'ajax';
			if ($tematicas_ids) {
				$conditions = array(
					'tematica_id' => explode(',', $tematicas_ids)
				);
			} else {
				$conditions = [];
			}
			$estados = $this->Estado->find(
				'all',
				array(
					'conditions' => $conditions,
					'order' => array(
						'Estado.nome' => 'asc'
					)
				)
			);
						
			echo json_encode($estados);
	
			$this->render(false);
		}
		
		public function ajax_municipios($estado_id = null) {
			
			$tematicas_ids = $this->request->query['tematicas'];
		
			$this->layout = 'ajax';
			$conditions = array();
			if ($estado_id) {
				array_push($conditions, array(
					'Municipio.estado_id' => $estado_id,
					)
				);
			};
			if ($tematicas_ids) {
				array_push($conditions, array(
					'Municipio.tematica_id' => explode(',', $tematicas_ids),
					)
				);
			};
			
			$this->Municipio->Behaviors->attach('Containable');
			$this->Municipio->contain(
				'Estado'
			);
			$municipios = $this->Municipio->find(
				'all',
				array(
					'conditions' => $conditions,
					'order' => array(
						'Estado.sigla' => 'asc',
						'Municipio.nome' => 'asc'
					)
				)
			);
			
			echo json_encode($municipios);
	
			$this->render(false);
		}
		
		public function ajax_ocorrencias($municipio_id = null, $estado_id = null) {
			$tematicas_ids = $this->request->query['tematicas'];
			if (isset($this->request->query['pesquisa']))
				$pesquisa_texto = $this->request->query['pesquisa'];
			else
				$pesquisa_texto = false;
			$this->layout = 'ajax';
			$conditions = [];
			if ($municipio_id) {
				array_push($conditions, ['Ocorrencia.municipio_id' => $municipio_id]);
			}
			if ($estado_id) {
				array_push($conditions, ['Municipio.estado_id' => $estado_id]);
			}
			if ($tematicas_ids) {
				array_push($conditions, array(
					'Municipio.tematica_id' => explode(',', $tematicas_ids),
					)
				);
			};
			if ($pesquisa_texto) {
				array_push($conditions, [
					'OR' => [
						'Programa.nome_oficial ilike' => '%'.$pesquisa_texto.'%',
						'Programa.nome_divulgacao ilike' => '%'.$pesquisa_texto.'%',
						'Municipio.nome like' => '%'.$pesquisa_texto.'%'
					]
				]);
			}
			
			$this->Ocorrencia->Behaviors->attach('Containable');
			$this->Ocorrencia->contain(
				'OcorrenciaOrgao.Orgao',
				'Localizacao',
				'Municipio.Estado',
				'Programa.OrgaoPrograma.Orgao'
			);
			$ocorrencias = $this->Ocorrencia->find(
				'all',
				array(
					'conditions' => $conditions
				)
			);
			
			
			
			echo json_encode($ocorrencias);
	
			$this->render(false);
		}
		
	}