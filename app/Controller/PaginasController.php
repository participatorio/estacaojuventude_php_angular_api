<?php
class PaginasController extends AppController {
	
	public function view($slug = 'home') {
		
		$pagina = $this->Pagina->find('first', array(
			'conditions' => array(
				'Pagina.slug' => $slug
			)
		));
		
		$this->set('pagina', $pagina);
		
	}
	
}