<?php
class ProgramasController extends AppController {
	
	public function index() {
		
		$tematicas = $this->Programa->TematicaAll->find('threaded');
		$this->set('tematicas', $tematicas);
		
		$programas = $this->Paginator->paginate('Programa',
			array(
				'Programa.status_id' => 1,
				'Programa.situacao_id' => 1
			)
		);
		$this->set('programas', $programas);
		
	}
	
	public function info($id) {
		$programa = $this->Programa->read(null, $id);
		$this->set('programa', $programa);
	}
	
}