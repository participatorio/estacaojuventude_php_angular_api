<?php
class OcorrenciasController extends AdminAppController {

	public $uses = array('Admin.Ocorrencia','Admin.Orgao');

	public function _related($id = 0) {
		$this->_getRelated('Status', true);
		$this->_getRelated('Situacao', true);
	}

	public function _getRelated($model, $none) {
		$options = $this->Ocorrencia->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none) {
			$options = array(''=>'Nenhum') + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}
	
	public function sessSearch() {
		if ($this->request->data) {
			$search = $this->request->data['search'];
			$this->Session->write('ocorrenciasSearch', $search);
		} else if ($search = $this->Session->read('ocorrenciasSearch')) {
			 $this->request->data['search'] = $search;
		}
		
		$conditions = [];
				
		if ($search['nome']) {
			$conditions['OR'] = [
				'Programa.nome_oficial ilike' => '%'.$search['nome'].'%',
				'Programa.nome_divulgacao ilike	' => '%'.$search['nome'].'%'
			];
		}
		
		if ($search['status_id']) {
			$conditions['Ocorrencia.status_id'] = $search['status_id'];
		}
		if ($search['situacao_id']) {
			$conditions['Ocorrencia.situacao_id'] = $search['situacao_id'];
		}
		
		return $conditions;
	}
	
	public function search_reset() {
		$this->Session->delete('ocorrenciasSearch');
		//$this->Session->setFlash('Busca zerada!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(['action'=>'index']);
	}

	public function index() {
		$this->_related();
		$conditions = $this->sessSearch();
		
		if ($this->user['Usuario']['estado_id']) {
			$conditions['Municipio.estado_id'] = $this->user['Usuario']['estado_id'];
		}
		if ($this->user['Usuario']['municipio_id']) {
			$conditions['Municipio.id'] = $this->user['Usuario']['municipio_id'];
		}
		$ocorrencias = $this->Paginator->paginate('Ocorrencia', $conditions);
		$this->set('ocorrencias', $ocorrencias);
	}

	public function add() {

		if ($this->request->is('post')) {
			$data = $this->request->data;
			$data['Ocorrencia']['status_id'] = 2;
			$data['Ocorrencia']['situacao_id'] = 2;
			
			if (!$this->Ocorrencia->save($data)) {
				$this->Session->setFlash('Desculpe, mas há erros de validação! Veja os erros encontrados nos campos abaixo.', 'alert-box', array('class'=>'alert-danger'));
			} else {
				$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
				$this->redirect(array('action'=>'index'));
			}
		}

		$this->_related();

		$programas = ['0'=>'Pesquise pelo Programa'];
		$this->set(Inflector::pluralize(strtolower('programas')), $programas);

		$municipios = ['0'=>'Pesquise pelo Município'];
		$this->set(Inflector::pluralize(strtolower('municipios')), $municipios);

		$this->render('form');
	}

	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$data['Ocorrencia']['status_id'] = 2;
			if (!$this->Ocorrencia->save($data)) {
				$this->Session->setFlash('Desculpe, mas há erros de validação! Veja os erros encontrados nos campos abaixo.', 'alert-box', array('class'=>'alert-danger'));
			} else {
			
				$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
				$this->redirect(array('action'=>'index'));
			}
		} else {
			$this->request->data = $this->_load($item_id);
		}
		$this->_related($item_id);

		$programas = $this->Ocorrencia->Programa->find(
			'list',
			[
				'order' => [],
				'fields' => ['id', 'nome_oficial'],
				'conditions' => [
					'Programa.nome_oficial like' => '%'.$this->request->data['Programa']['nome_oficial'].'%'
				],
				'limit' => 10
			]
		);
		$this->set(Inflector::pluralize(strtolower('programas')), $programas);

		$municipios = $this->Ocorrencia->Municipio->find(
			'list',
			[
				'order' => [],
				'fields' => ['id', 'nome'],
				'conditions' => [
					'Municipio.nome like' => '%'.$this->request->data['Municipio']['nome'].'%'
				],
				'limit' => 10
			]
		);
		$this->set(Inflector::pluralize(strtolower('municipios')), $municipios);
		
		$this->render('form');
	}

	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Ocorrencia->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}

	public function _load($item_id) {
		$ocorrencia = $this->Ocorrencia->read(null, $item_id);
		return $ocorrencia;
	}

	public function view($id) {
		$this->request->data = $this->Ocorrencia->read(null, $id);
	}
	
	public function orgaos($ocorrencia_id) {
		if ($this->request->is('post')) {
			$this->save_orgaos($ocorrencia_id);
		}
		
		$ocorrencia = $this->_load($ocorrencia_id);
		
		$orgao_ocorrencias = $this->Orgao->OcorrenciaOrgao->find(
			'all',
			[
				'conditions' => [
					'OcorrenciaOrgao.ocorrencia_id' => $ocorrencia_id
				]
			]
		);
		$this->set('orgaos_ocorrencias', $orgao_ocorrencias);
		
		$orgaos_ids = [];
		foreach($orgao_ocorrencias as $item) {
			array_push($orgaos_ids, $item['OcorrenciaOrgao']['orgao_id']);
		}
		
		$orgaos = $this->Orgao->find('all',[
			'conditions' => [
				'not' => [
					'Orgao.id' => $orgaos_ids
				],
				'and' => [
					'Orgao.municipio_id' => $ocorrencia['Ocorrencia']['municipio_id']
				]
			]
		]);
		$this->set('orgaos', $orgaos);
		$this->set('ocorrencia', $ocorrencia);
	}
	
	public function save_orgaos($ocorrencia_id) {
		$data = $this->request->data;
		
		$conditions = [
			'OcorrenciaOrgao.ocorrencia_id' => $ocorrencia_id
		];
		pr($data);
		$this->Orgao->OcorrenciaOrgao->deleteAll($conditions);
		
		foreach($data['OcorrenciaOrgao']['orgao_id'] as $orgao_id) {
			$orgao = [
				'ocorrencia_id' => $ocorrencia_id,
				'orgao_id' => $orgao_id
			];
			$this->Orgao->OcorrenciaOrgao->create();
			$this->Orgao->OcorrenciaOrgao->save($orgao);
		}
		$this->Session->setFlash('Órgão relacionadas com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}
	
	public function ativa_desativa($id, $situacao_id) {
		if ($situacao_id == 2) {
			$new_situacao_id = 1;
		} else {
			$new_situacao_id = 2;
		}
		$ocorrencia = [
			'id' => $id,
			'situacao_id' => $new_situacao_id
		];
		$this->Ocorrencia->save($ocorrencia);
		$this->Session->setFlash('Situação do Ocorrência alterada com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));

		
	}
	
	public function doValidate($id) {
		if ($this->user['Grupo']['id'] > 2) {
			$this->Session->setFlash('Acesso restrito!', 'alert-box', array('class'=>'alert-warning'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->user['Grupo']['id'] == 1) {
			$ocorrencia = [
				'id' => $id,
				'status_id' => 1
			];
		} else if ($this->user['Grupo']['id'] == 2) {
			$ocorrencia = [
				'id' => $id,
				'status_id' => 3
			];
		}
		$this->Ocorrencia->save($ocorrencia);
		$this->Session->setFlash('Ocorrência Validada com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}
	
	public function doReject($id) {
		if ($this->user['Grupo']['id'] > 2) {
			$this->Session->setFlash('Acesso restrito!', 'alert-box', array('class'=>'alert-warning'));
			$this->redirect(array('action'=>'index'));
		}
		
		$ocorrencia = [
			'id' => $id,
			'status_id' => 4
		];
		
		$this->Ocorrencia->save($ocorrencia);
		$this->Session->setFlash('Ocorrência Rejeitada com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}

	public function programa_ajax() {
		$this->layout = 'ajax';
		$data = $this->request->data;
		if (isset($data['nome'])) {
			$conditions = [
				"Programa.nome_oficial ilike"=>'%'.strtolower( $data['nome'] ).'%',
				'Programa.status_id' => 1,
				'Programa.situacao_id' => 1
			];
		} else {
			$conditions = [
				'Programa.status_id' => 1,
				'Programa.situacao_id' => 1
			];
		}

		$programas = $this->Ocorrencia->Programa->find(
			'all',
			[
				'conditions' => $conditions,
				'limit' => 20
			]
		);

		echo json_encode($programas);
		$this->render(false);

	}

	public function municipio_ajax() {
		$this->layout = 'ajax';
		$data = $this->request->data;
		if (isset($data['nome'])) {
			$conditions = [
				'Municipio.nome ilike'=>'%'. strtolower( $data['nome'] ).'%'
			];
		} else {
			$conditions = [];
		}

		$municipios = $this->Ocorrencia->Municipio->find(
			'all',
			[
				'conditions' => $conditions,
				'limit' => 10
			]
		);

		echo json_encode($municipios);
		$this->render(false);

	}
	
	


}
