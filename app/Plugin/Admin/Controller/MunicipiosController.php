<?php
class MunicipiosController extends AdminAppController {
	
	public $uses = array('Admin.Municipio');
	
	public function _related($id = 0) {
	}
	
	public function _getRelated($model, $none) {
		$options = $this->Nivel->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none) {
			$options = array(''=>'Nenhum') + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}
	
	public function index() {
		$municipios = $this->Paginator->paginate('Municipio');
		$this->set('municipios', $municipios);
	}
	
	public function add() {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$this->Municipio->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related();
		$this->render('form');
	}
	
	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$this->Municipio->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related($item_id);
		
		$this->request->data = $this->_load($item_id);
		$this->render('form');
	}
	
	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Municipio->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Municipio->read(null, $item_id);
	}
	
}