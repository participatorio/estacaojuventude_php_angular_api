<?php
class MensagensController extends AdminAppController {
	
	public $uses = array('Admin.Mensagem');
	
	public function index() {
		
	}
	
	public function pastas_ajax() {
		
		$this->layout = 'ajax';
		
		$user = $this->Session->read('user');
		
		$pastas = $this->Mensagem->Pasta->find(
			'all',
			[
				'conditions' => [
					'OR' => [
						'Pasta.usuario_id is NULL',
						'Pasta.usuario_id' => $user['Usuario']['id']
					]
				],
				'order' => [
					'Pasta.id' => 'ASC'
				]
			]
		);
		echo json_encode($pastas);
		
		$this->render(false);
	}
	
	public function mensagens_ajax($pasta_id = 1) {
		
		$this->layout = 'ajax';
		
		$user = $this->Session->read('user');
		$conditions = [
			'Mensagem.dono_id' => $user['Usuario']['id'],
			'Mensagem.pasta_id' => $pasta_id
		];
		$this->Mensagem->Behaviors->attach('Containable');
		$this->Mensagem->contain(
			'Remetente.Grupo',
			'Remetente.Municipio.Estado',
			'Destinatario.Grupo',
			'Destinatario.Municipio.Estado'
		);
		$mensagens = $this->Mensagem->find(
			'all',
			[
				'conditions' => $conditions
			]
		);
		echo json_encode($mensagens);
		
		$this->render(false);
	}
	
}