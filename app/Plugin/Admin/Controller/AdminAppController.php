<?php
class AdminAppController extends AppController {

	public $components = array('Session');

	public function beforeFilter() {
		

		if (!$this->Session->check('user')) {
			// Se o usuário não estiver logado

			if ($this->name != 'Auths' && $this->action != 'doLogin') {
				// Se não estiver acessando o formulário de Login
				$this->layout = 'login';
				// Carrega o Layout de Login
				$this->render('Admin.Auths/do_login');
				// Carrega o Form de Login
			}
		} else {
			// Se não, e estiver logado.
			$user = $this->Session->read('user');
			// Ler dados de usuário da sessão
			$this->user = $user;
			// Criar um objeto "user" no Controller Principal do Admin com os dados do Usuário logado
			$this->set('user', $user);
			// Enviar os dados do Usuário logado para a view
		}

		parent::beforeFilter();
		// Carrega o metodo beforeFilter o AppController
	}

}
