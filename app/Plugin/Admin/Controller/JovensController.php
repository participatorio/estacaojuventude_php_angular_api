<?php
class JovensController extends AdminAppController {
	
	public $uses = array('Admin.Jovem');
	
	public function _related($id = 0) {
		$this->_getRelated('Status', true);
		$this->_getRelated('Nivel', true);
		$this->_getRelated('Situacao', true);
		$this->_getRelated('Temporalidade', true);
	}
	
	public function _getRelated($model, $none) {
		$options = $this->Programa->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none) {
			$options = array(''=>'Nenhum') + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}
	
	public function index() {
		$jovens = $this->Paginator->paginate('Jovem');
		$this->set('jovens', $jovens);
	}
	
	public function add() {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$this->Tematica->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related();
		$this->render('form');
	}
	
	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$this->Programa->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related($item_id);
		
		$this->request->data = $this->_load($item_id);
		$this->render('form');
	}
	
	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Tematica->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Programa->read(null, $item_id);
	}
	
}