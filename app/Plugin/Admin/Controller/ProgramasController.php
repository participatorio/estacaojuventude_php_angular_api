<?php
class ProgramasController extends AdminAppController {
	
	public $uses = array('Admin.Programa','Admin.Tematica', 'Admin.Orgao');
	
	public function _related($id = 0) {
		//$this->_getRelated('Status', false);
		//$this->_getRelated('Nivel', false);
		$this->_getRelated('Situacao', false);
		$this->_getRelated('Temporalidade', false);
	}
	
	public function _getRelated($model, $none) {
		$options = $this->Programa->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none != false) {
			$options = array(''=>$none) + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}
	
	public function userConditions() {
		$conditions = [];
		if ($this->user['Usuario']['estado_id']) {
			array_push($conditions, ['Programa.estado_id'=>$this->user['Usuario']['estado_id']]);
		}
		if ($this->user['Usuario']['municipio_id']) {
			array_push($conditions, ['Programa.municipio_id'=>$this->user['Usuario']['municipio_id']]);
		}
		
		return $conditions;
	}
	
	public function sessSearch() {
		if ($this->request->data) {
			$search = $this->request->data['search'];
			$this->Session->write('programasSearch', $search);
		} else if ($search = $this->Session->read('programasSearch')) {
			 $this->request->data['search'] = $search;
		} else {
			return [];
		}
		
		$conditions = [];
				
		if ($search['nome']) {
			$conditions['OR'] = [
				'nome_oficial ilike' => '%'.$search['nome'].'%',
				'nome_divulgacao ilike	' => '%'.$search['nome'].'%'
			];
		}
		
		if ($search['status_id']) {
			$conditions['status_id'] = $search['status_id'];
		}
		if ($search['situacao_id']) {
			$conditions['situacao_id'] = $search['situacao_id'];
		}
		return $conditions;
	}
	
	
	public function search_reset() {
		$this->Session->delete('programasSearch');
		//$this->Session->setFlash('Busca reiniciada!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(['action'=>'index']);
	}
	
	public function index() {
		$this->_getRelated('Status', 'Todos');
		$this->_getRelated('Situacao', 'Todas');
		$this->_getRelated('Nivel', 'Todos');

		$conditions = $this->sessSearch();
		$conditions += $this->userConditions();
		
		$programas = $this->Paginator->paginate('Programa', $conditions);
		
		$this->set('programas', $programas);
	}
	
	public function add() {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			
			$data['Programa']['situacao_id'] = 2;
			$data['Programa']['status_id'] = 2;
			
			$data['Programa']['estado_id'] = $this->user['Usuario']['estado_id'];
			$data['Programa']['municipio_id'] = $this->user['Usuario']['municipio_id'];
			
			if (!$this->Programa->save($data)) {
				$this->Session->setFlash('Desculpe, mas há erros de validação! Veja os erros encontrados nos campos abaixo.', 'alert-box', array('class'=>'alert-danger'));
			} else {
				$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
				$this->redirect(['action'=>'edit', $this->Programa->id]);
			}
		}
		
		$this->_related();
		if ($this->user['Grupo']['admin']) {
			$this->set('nivels', 
				[
					'1' => 'Nacional',
					'2' => 'Estadual',
					'3' => 'Municipal'
				]
			);
		} else {
			$this->set('nivels', 
				[
					'2' => 'Estadual',
					'3' => 'Municipal'
				]
			);
		}
		$this->render('form');
	}
	
	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$data['Programa']['status_id'] = 2;
			if (!$this->Programa->save($data)) {
				$this->Session->setFlash('Desculpe, mas há erros de validação! Veja os erros encontrados nos campos abaixo.', 'alert-box', array('class'=>'alert-danger'));
			} else {
				$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
				$this->redirect(array('action'=>'index'));
			}
		} else {
			$this->request->data = $this->_load($item_id);
		}
		if ($this->user['Grupo']['admin']) {
			$this->set('nivels', 
				[
					'1' => 'Nacional',
					'2' => 'Estadual',
					'3' => 'Municipal'
				]
			);
		} else {
			$this->set('nivels', 
				[
					'2' => 'Estadual',
					'3' => 'Municipal'
				]
			);
		}
		$this->_related($item_id);
		$this->render('form');
	}
	
	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Programa->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Programa->read(null, $item_id);
	}
	
	public function tematicas($programa_id) {
		if ($this->request->is('post')) {
			$this->save_tematicas($programa_id);
		}
		$programa_tematicas = $this->Tematica->ProgramaTematica->find(
			'all',
			[
				'conditions' => [
					'ProgramaTematica.programa_id' => $programa_id
				]
			]
		);
		$this->set('programas_tematicas', $programa_tematicas);
		
		$tematicas_ids = [];
		foreach($programa_tematicas as $item) {
			array_push($tematicas_ids, $item['ProgramaTematica']['tematica_id']);
		}
		
		$tematicas = $this->Tematica->find('all',[
			'conditions' => [
				'not' => [
					'Tematica.id' => $tematicas_ids
				]
			]
		]);
		$this->set('tematicas', $tematicas);
		$this->set('programa', $this->Programa->read(['id','nome_oficial'], $programa_id));
	}
	
	public function save_tematicas($programa_id) {
		$data = $this->request->data;
		
		$conditions = [
			'ProgramaTematica.programa_id' => $programa_id
		];
		$this->Tematica->ProgramaTematica->deleteAll($conditions);
		
		foreach($data['ProgramaTematica']['tematica_id'] as $tematica_id) {
			$tematica = [
				'programa_id' => $programa_id,
				'tematica_id' => $tematica_id
			];
			$this->Tematica->ProgramaTematica->create();
			$this->Tematica->ProgramaTematica->save($tematica);
		}
		$this->Session->setFlash('Temáticas relacionadas com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}
	
	public function view($id) {
		$this->request->data = $this->Programa->read(null, $id);
	}
	
	public function orgaos($programa_id) {
		if ($this->request->is('post')) {
			$this->save_orgaos($programa_id);
		}
		$orgao_programas = $this->Orgao->OrgaoPrograma->find(
			'all',
			[
				'conditions' => [
					'OrgaoPrograma.programa_id' => $programa_id
				]
			]
		);
		$this->set('orgaos_programas', $orgao_programas);
		
		$orgaos_ids = [];
		foreach($orgao_programas as $item) {
			array_push($orgaos_ids, $item['OrgaoPrograma']['orgao_id']);
		}
		
		$orgaos = $this->Orgao->find('all',[
			'conditions' => [
				'not' => [
					'Orgao.id' => $orgaos_ids
				]
			]
		]);
		$this->set('orgaos', $orgaos);
		$this->set('programa', $this->Programa->read(['id','nome_oficial'], $programa_id));
	}
	
	public function save_orgaos($programa_id) {
		$data = $this->request->data;
		
		$conditions = [
			'OrgaoPrograma.programa_id' => $programa_id
		];
		pr($data);
		$this->Orgao->OrgaoPrograma->deleteAll($conditions);
		
		foreach($data['OrgaoPrograma']['orgao_id'] as $orgao_id) {
			$orgao = [
				'programa_id' => $programa_id,
				'orgao_id' => $orgao_id
			];
			$this->Orgao->OrgaoPrograma->create();
			$this->Orgao->OrgaoPrograma->save($orgao);
		}
		$this->Session->setFlash('Órgão relacionadas com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}
	
	public function ativa_desativa($id, $situacao_id) {
		if ($situacao_id == 2) {
			$new_situacao_id = 1;
		} else {
			$new_situacao_id = 2;
		}
		$programa = [
			'id' => $id,
			'situacao_id' => $new_situacao_id
		];
		$this->Programa->save($programa);
		$this->Session->setFlash('Situação do Programa alterada com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));

		
	}
	
	public function doValidate($id) {
		if ($this->user['Grupo']['id'] > 2) {
			$this->Session->setFlash('Acesso restrito!', 'alert-box', array('class'=>'alert-warning'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->user['Grupo']['id'] == 1) {
			$programa = [
				'id' => $id,
				'status_id' => 1
			];
		} else if ($this->user['Grupo']['id'] == 2) {
			$programa = [
				'id' => $id,
				'status_id' => 3
			];
		}
		$this->Programa->save($programa);
		$this->Session->setFlash('Registro salvo com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}
	
	public function doReject($id) {
		if ($this->user['Grupo']['id'] > 2) {
			$this->Session->setFlash('Acesso restrito!', 'alert-box', array('class'=>'alert-warning'));
			$this->redirect(array('action'=>'index'));
		}
		
		$programa = [
			'id' => $id,
			'status_id' => 4
		];
		
		$this->Programa->save($programa);
		$this->Session->setFlash('Registro salvo com sucesso!', 'alert-box', array('class'=>'alert-success'));
		$this->redirect(array('action'=>'index'));
		
	}

}