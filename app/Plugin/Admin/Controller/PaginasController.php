<?php
class PaginasController extends AdminAppController {
	
	public $uses = array('Admin.Pagina');
	
	public function _related($id = 0) {
		
	}
	
	public function _getRelated($model, $none) {
	}
	
	public function index() {
		$paginas = $this->Paginator->paginate('Pagina');
		$this->set('paginas', $paginas);
	}
	
	public function add() {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$this->Pagina->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related();
		$this->render('form');
	}
	
	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$this->Pagina->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related($item_id);
		
		$this->request->data = $this->_load($item_id);
		$this->set('imagens', $this->Pagina->Imagem->find('all'));
		$this->render('form');
	}
	
	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Pagina->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}
	
	public function upload($id = null) {
		$this->layout = 'ajax';
		$data = $_FILES;
		$imagem = [
			'descricao' => $data['file']['name'],
			'tipo' => $data['file']['type'],
			'arquivo' => $data['file']['name'],
			'pagina_id' => $id
		];
		$img = $this->Pagina->Imagem->save($imagem);
		move_uploaded_file($data['file']['tmp_name'], APP.'webroot/files/paginas/p_'.$id.'_'.$img['Imagem']['id']);
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Pagina->read(null, $item_id);
	}
	
}