<?php
	class AuthsController extends AdminAppController {

		public $uses = ['Admin.Usuario'];

		public function doLogin() {
			App::uses('Security', 'Utility');
			$this->layout = 'login';
			$data = $this->request->data;
			$conditions = [
				'Usuario.login' => $data['Auth']['login'],
				'Usuario.senha' => Security::hash( $data['Auth']['senha'] )
			];
			$this->Usuario->Behaviors->attach('Containable');

			$usuario = $this->Usuario->find(
				'first',
				[
					'conditions' => $conditions
				]
			);
			if ($usuario) {

				$this->Session->write('user', $usuario);
				$this->Session->setFlash('Usuário conectado com sucesso!', 'alert-box', array('class'=>'alert-success'));
				$this->redirect('/admin');
			} else {
				$this->Session->delete('user');
				$this->Session->setFlash('Erro ao se conectar!', 'alert-box', array('class'=>'alert-danger'));
				pr(Security::hash( $data['Auth']['senha'] ));
			}

		}
		
		public function profile() {
			if ($this->request->isPost()) {
				$data = $this->request->data;
				if ($data['Usuario']['senha'] != '') {
					$data['Usuario']['id'] = $this->user['Usuario']['id'];
					$data['Usuario']['senha'] = Security::hash( $data['Usuario']['senha'] );
				}
				$this->Usuario->save($data);
				$this->Session->setFlash('Senha alterada com sucesso!', 'alert-box', array('class'=>'alert-success'));
				$this->redirect('/admin');
			}
		}
		

		public function logout() {
			$this->Session->delete('user');
			$this->Session->setFlash('Usuário desconectado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect('/admin');
		}

	}
