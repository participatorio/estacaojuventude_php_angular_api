<?php
class LocalizacoesController extends AdminAppController {
	
	public $uses = array('Admin.Localizacao');
	
	public function _related($id = 0) {

	}
	
	public function _getRelated($model, $none) {
		$options = $this->Localizacao->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none) {
			$options = array(''=>'Nenhum') + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}
	
	public function index($ocorrencia_id = null) {
		if (!$ocorrencia_id) {
			$this->redirect(array('controller'=>'Ocorrencias'));
		}
		$localizacoes = $this->Localizacao->find('all', array(
			'conditions' => array(
				'Localizacao.ocorrencia_id' => $ocorrencia_id
			)
		));
		$this->set('ocorrencia_id', $ocorrencia_id);
		$this->set('localizacoes', $localizacoes);
	}
	
	public function add($ocorrencia_id) {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$data['Localizacao']['ocorrencia_id'] = $ocorrencia_id;
			$this->Localizacao->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('controller'=>'localizacoes','action'=>'index', $ocorrencia_id));
		}
		
		$ocorrencia = $this->Localizacao->Ocorrencia->read(null, $ocorrencia_id);
		$this->set('municipio', $ocorrencia['Municipio']);
		
		$this->_related();
		$this->render('form');
	}
	
	public function edit($id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$this->Localizacao->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('controller'=>'localizacoes','action'=>'index', $data['Localizacao']['ocorrencia_id']));
		}
		
		
		$this->_related($id);
		
		$this->request->data = $this->_load($id);
		$this->render('form');
	}
	
	public function del($id, $ocorrencia_id) {
		if ($this->request->is('post')) {
			$this->Localizacao->delete($id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('controller'=>'localizacoes','action'=>'index', $ocorrencia_id));
		}
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Localizacao->read(null, $item_id);
	}
	
}