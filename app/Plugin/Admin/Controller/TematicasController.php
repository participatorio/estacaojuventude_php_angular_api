<?php
class TematicasController extends AdminAppController {
	
	public function _related($id = 0) {
		$options = $this->Tematica->find('list',
			array(
				'conditions'=>array('parent_id'=>null,'not'=>array('id'=>$id)),
				'fields' => array('id','nome')
			)
		);
		$this->set('parents', array(''=>'Nenhuma') + $options);
	}
	
	public function index() {
		$tematicas = $this->Tematica->find('threaded');
		$this->set('tematicas', $tematicas);
	}
	
	public function add() {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$this->Tematica->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related();
		$this->render('form');
	}
	
	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$this->Tematica->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related($item_id);
		
		$this->request->data = $this->_load($item_id);
		$this->render('form');
	}
	
	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Tematica->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Tematica->read(null, $item_id);
	}
	
}