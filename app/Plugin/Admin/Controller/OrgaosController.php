<?php
class OrgaosController extends AdminAppController {
	
	public $uses = array('Admin.Orgao');
	
	public function _related($id = 0) {
		
	}
	
	public function _getRelated($model, $none) {
		$options = $this->Nivel->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none) {
			$options = array(''=>'Nenhum') + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}
	
	public function index() {
		$orgaos = $this->Paginator->paginate('Orgao');
		$this->set('orgaos', $orgaos);
	}
	
	public function add() {
		
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$this->Orgao->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related();
		$this->render('form');
	}
	
	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			$this->Orgao->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->_related($item_id);
		
		$this->request->data = $this->_load($item_id);
		$this->render('form');
	}
	
	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Orgao->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}
	
	public function _load($item_id) {
		return $this->Orgao->read(null, $item_id);
	}
	
	public function estado_ajax() {
		$this->layout = 'ajax';
		$data = $this->request->data;
		if (isset($data['nome'])) {
			$conditions = [
				'OR' => [
					'Estado.nome ilike'=>'%'. strtolower( $data['nome'] ).'%',
					'Estado.sigla ilike'=>'%'. strtolower( $data['nome'] ).'%',
				]
			];
		} else {
			$conditions = [];
		}

		$estados = $this->Orgao->Estado->find(
			'all',
			[
				'conditions' => $conditions,
				'limit' => 10
			]
		);

		echo json_encode($estados);
		$this->render(false);

	}
	
	public function municipio_ajax() {
		$this->layout = 'ajax';
		$data = $this->request->data;
		if (isset($data['nome'])) {
			$conditions = [
				'Municipio.nome ilike'=>'%'. strtolower( $data['nome'] ).'%'
			];
		} else {
			$conditions = [];
		}

		$municipios = $this->Orgao->Municipio->find(
			'all',
			[
				'conditions' => $conditions,
				'limit' => 10
			]
		);

		echo json_encode($municipios);
		$this->render(false);

	}
	
}