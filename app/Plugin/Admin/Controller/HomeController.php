<?php
class HomeController extends AdminAppController {
	
	public $uses = ['Admin.Mensagem','Admin.Ocorrencia','Admin.Programa'];
	
	public function index() {
		
		$mensagens = $this->Mensagem->find(
			'all', 
			[
				'limit'=>10,
				'conditions' => [
					'Mensagem.lida' => 0,
					'Mensagem.destinatario_id' => $this->user['Usuario']['id']
				]
			]
		);
		
		// Mensagens
		$this->set(
			'mensagens', $mensagens 
		);
		
		
		// Validação
		$conditions_validar = [];
		$conditions_validar_p = [];
		
		if ($this->user['Grupo']['admin']) {
			$conditions_validar['Ocorrencia.status_id'] = '3';
			$conditions_validar_p['Programa.status_id'] = '3';
		} else if ($this->user['Grupo']['id'] == 2) {
			$conditions_validar['Ocorrencia.status_id'] = '2';
			$conditions_validar_p['Programa.status_id'] = '2';
		}
		
		$this->set('ocorrencias_validar', $this->Ocorrencia->find('all', 
			[
				'limit'=>10,
				'conditions' => $conditions_validar,
				'recursive' => 2
			]
		) );
		
		$programas_validar = $this->Programa->find('all', 
			[
				'limit'=>10,
				'conditions' => $conditions_validar_p,
				'recursive' => 2
			]
		);
		
		$this->set('programas_validar', $programas_validar );
		
		// A Vencer
		$conditions_vencer = [];
		
		$semana = date('Y-m-d', strtotime('+1 Week'));
		$conditions_vencer['Ocorrencia.fim_inscricoes <='] = $semana;
		
		
		$this->set('ocorrencias_vencer', $this->Ocorrencia->find('all', 
			[
				'limit'=>10,
				'conditions' => $conditions_vencer,
				'recursive' => 2
			]
		) );


		// Rejeitadas
		$conditions_rejeitada = [];
		$conditions_rejeitada_p = [];
		
		$conditions_rejeitada['Ocorrencia.status_id'] = '4';
		$conditions_rejeitada_p['Programa.status_id'] = '4';
				
		$this->set('ocorrencias_rejeitadas', $this->Ocorrencia->find('all', 
			[
				'limit'=>10,
				'conditions' => $conditions_rejeitada,
				'recursive' => 2
			]
		) );
		
		$this->set('programas_rejeitadas', $this->Programa->find('all', 
			[
				'limit'=>10,
				'conditions' => $conditions_rejeitada_p,
				'recursive' => 2
			]
		) );

	}
	
}