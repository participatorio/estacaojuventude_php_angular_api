<?php
class UsuariosController extends AdminAppController {

	public $uses = array('Admin.Usuario');
	
	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function _related($id = 0) {
		$this->set('grupos', $this->Usuario->Grupo->find('list', ['fields'=>['id','nome']]) );
		$this->set('estados', [''=>'Nenhum'] + $this->Usuario->Estado->find('list', ['fields'=>['id','sigla']]) );
		$this->set('municipios', [''=>'Nenhum']);
	}
	
	public function pesquisa_municipios() {
		$this->set('municipios', [''=>'Nenhum'] + $this->Usuario->Municipio->find('list', ['fields'=>['id','nome'],'limit'=>'10']) );
	}

	public function _getRelated($model, $none) {
		$options = $this->Usuario->{$model}->find('list',
			array(
				'fields' => array('id','nome')
			)
		);
		if ($none) {
			$options = array(''=>'Nenhum') + $options;
		}
		$this->set(Inflector::pluralize(strtolower($model)), $options);
	}

	public function index() {
		$usuarios = $this->Paginator->paginate('Usuario');
		$this->set('usuarios', $usuarios);
	}

	public function add() {

		if ($this->request->is('post')) {
			$data = $this->request->data;
			$data['Usuario']['senha'] = Security::hash( $data['Usuario']['senha'] );
			$this->Usuario->save($data);
			$this->Session->setFlash('Registro criado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}

		$this->_related();
		$this->render('form');
	}

	public function edit($item_id) {
		if ($this->request->is('put')) {
			$data = $this->request->data;
			if ($data['Usuario']['senha'] == '') {
				unset($data['Usuario']['senha']);
			} else {
				$data['Usuario']['senha'] = Security::hash( $data['Usuario']['senha'] );
			}
			$this->Usuario->save($data);
			$this->Session->setFlash('Registro editado com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}

		$this->_related($item_id);

		$usuario = $this->_load($item_id);
		unset($usuario['Usuario']['senha']);
		$this->request->data = $usuario;
		$this->render('form');
	}

	public function del($item_id) {
		if ($this->request->is('post')) {
			$this->Usuario->delete($item_id);
			$this->Session->setFlash('Registro excluído com sucesso!', 'alert-box', array('class'=>'alert-success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->render(false);
	}

	public function _load($item_id) {
		return $this->Usuario->read(null, $item_id);
	}
	
	public function municipio_ajax() {
		$this->layout = 'ajax';
		$data = $this->request->data;
		if (isset($data['nome'])) {
			$conditions = [
				'Municipio.nome ilike'=>'%'. strtolower( $data['nome'] ).'%'
			];
		} else {
			$conditions = [];
		}

		$municipios = $this->Usuario->Municipio->find(
			'all',
			[
				'conditions' => $conditions,
				'limit' => 10
			]
		);

		echo json_encode($municipios);
		$this->render(false);

	}

}
