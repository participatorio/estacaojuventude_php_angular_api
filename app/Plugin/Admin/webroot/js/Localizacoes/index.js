estacaoApp.cC({
	name: 'localizacaoCtrl',
	inject: ['$scope'],
	data: {
	},
	init: function() {
		this.$.marker = {
			
			id: 0,
			coords: {
			},
			options: { draggable: true }
		};
		this.$.map = {
			center: {
				latitude: -14,
				longitude: -49
			},
			zoom: 15,
			options: {
				draggable: 1,
				panControl: 0,
				mapTypeControl: 0
			}
		};
	},
	methods: {
		
	}
});