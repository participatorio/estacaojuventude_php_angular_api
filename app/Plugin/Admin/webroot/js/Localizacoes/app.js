var estacaoApp = angular.module('estacaoApp', ['classy','ngResource','uiGmapgoogle-maps']);
estacaoApp.config(function(uiGmapGoogleMapApiProvider) {
	uiGmapGoogleMapApiProvider.configure({
			language: 'pt_BR'
	});
})