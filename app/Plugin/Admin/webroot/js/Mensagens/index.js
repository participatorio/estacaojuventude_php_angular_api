estacaoApp.cC({
	name: 'mensagemCtrl',
	inject: ['$scope', '$resource','$sanitize'],
	data: {
		selecteds: {},
		escrevendo: false,
		mensagemVazia: {
			destinatario_id: null,
			assunto: '',
			body: ''
		}
	},
	init: function() {
		var vm = this;
		vm.getFolders();
		vm.getMessages();
		vm.$.pasta_id = 1;
		vm.$.destList = [
				{
					nome: 'Humberto Cruz',
					grupo: '(Admin)',
					ticked: false
				},
				{
					nome: 'Sarah Ceratti',
					grupo: '(Coordenador Geral)',
					ticked: false
				}
			];
			vm.$.localLang = {
				selectAll       : "Selecione Tudo",
				selectNone      : "Selecione Nenhum",
				reset           : "Reverter",
				search          : "Pesquise pelo nome...",
				nothingSelected : "Adicionar os Destinários aqui."
			};

	},
	methods: {
		getFolders: function() {
			var vm = this;
			this.$resource('/admin/mensagens/pastas_ajax').query().$promise
				.then(function(result) {
					vm.$.pastas = result;
					vm.selecteds.Folder = vm.$.pastas[0].Pasta;
			});
			
		},
		getMessages: function($pasta_id) {
			var vm = this;
			if ($pasta_id == undefined) {
				url = '/admin/mensagens/mensagens_ajax/1';
			} else {
				url = '/admin/mensagens/mensagens_ajax/'+$pasta_id
			}
			this.$resource(url).query().$promise
				.then(function(result) {
					vm.$.mensagens = result;
			});
		},
		changeFolder: function(item) {
			var vm = this;
			delete(vm.$.selectedMessage);
			vm.$.escrevendo = false;
			vm.$.escrevendoNova = false;
			vm.selecteds.Folder = item.Pasta;
			vm.getMessages(item.Pasta.id);
		},
		showMessage: function(item) {
			var vm = this;
			vm.$.escrevendo = false;
			vm.$.escrevendoNova = false;
			vm.$.selectedMessage = item.Mensagem;
		},
		nova: function() {
			var vm = this;
			delete(vm.$.selectedMessage);
			vm.$.escrevendo = false;
			vm.$.escrevendoNova = true;
			
			vm.$.destSelected = [];
		},
		responder: function() {
			var vm = this;
			vm.$.escrevendo = true;
			vm.$.escrevendoNova = false;
			vm.$.mensagem = vm.mensagemVazia;
			vm.$.mensagem.destinatario_id = vm.$.selectedMessage.remetente_id;
			vm.$.mensagem.assunto = 'Res.: '+vm.$.selectedMessage.assunto;
			vm.$.mensagem.body = vm.$.selectedMessage.body;
			
			vm.$.destSelected = [];
		},
		encaminhar: function() {
			var vm = this;
			vm.$.escrevendo = true;
			vm.$.escrevendoNova = false;
			vm.$.mensagem = vm.mensagemVazia;
			vm.$.mensagem.assunto = 'Enc.: '+vm.$.selectedMessage.assunto;
			vm.$.mensagem.body = vm.$.selectedMessage.body;
			
			vm.$.destSelected = [];
		},
		enviar: function() {
			var vm = this;
			console.log(vm.$.destSelected);
		}
	}
});