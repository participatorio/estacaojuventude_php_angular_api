estacaoApp.cC({
	name: 'paginaCtrl',
	inject: ['$scope','FileUploader'],
	init: function() {
		this.$.uploader = new this.FileUploader({
			url: '/admin/paginas/upload/'+$('#PaginaId').val(),
			autoUpload: true,
			removeAfterUpload: true
		});
	}
});