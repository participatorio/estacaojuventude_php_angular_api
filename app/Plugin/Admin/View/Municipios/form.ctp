<div class="content-header">
Formulário Programa
</div>

<div class="content body">
	<div class="box box-primary">
		<?php echo $this->Form->create('Municipio'); ?>
		<?php echo $this->Form->input('id'); ?>
		<div class="box-header">
			<h3 class="box-title">Programa</h3>
			<div class="box-tools pull-right">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<div class="box-body">

			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('nome', array('label'=>'Nome')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('nome_divulgacao', array('label'=>'Nome')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('sigla', array('label'=>'Sigla')); ?>
				</div>
			</div>
		</div>
		
		<?php echo $this->Form->end(); ?>
	</div>
</div>
