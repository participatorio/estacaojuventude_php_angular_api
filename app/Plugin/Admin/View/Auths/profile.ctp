<div class="content body">
	<div class="page-header">
		<h3>Profile do Usuário</h3>
	</div>
	
	<?php echo $this->Form->create('Usuario'); ?>
	
		<div class="box box-default">
			
			<div class="box-body">
				
				<div class="row">
					<div class="col-md-6">
						<?php echo $this->Form->input('senha', ['type'=>'password']); ?>
					</div>
					<div class="col-md-6">
						<?php echo $this->Form->input('senha2', ['type'=>'password','label'=>'Confirmação']); ?>
					</div>
				</div>
			</div>
			
			<div class="box-footer clearfix">
				
				<div class="btn-group pull-right">
					
					<?php echo $this->Form->submit('Alterar Senha'); ?>
				</div>
				
			</div>
			
		</div>
	
	
	<?php echo $this->Form->end(); ?>
	
</div>
