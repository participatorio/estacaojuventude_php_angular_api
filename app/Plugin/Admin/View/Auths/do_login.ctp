<div class="spacer" style="margin-top: 200px;"></div>
<div class="row">
	<div class="col-md-4 col-md-push-4">
		<div class="box box-primary box-solid">
			<div class="box-header">
				<h3 class="box-title">Login</h3>
			</div>
			<?php echo $this->Form->create('Auth', ['action'=>'doLogin']); ?>
			<div class="box-body">
				<?php echo $this->Form->input('login');?>
				<?php echo $this->Form->input('senha', ['type'=>'password']);?>
			</div>
			<div class="box-footer">
				<div class="btn-group pull-right">
					<button type="submit" class="btn btn-primary">Conectar</button>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
