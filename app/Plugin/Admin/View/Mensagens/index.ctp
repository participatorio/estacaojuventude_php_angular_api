<?php
	echo $this->Html->script('/bower_components/angular/angular.min');
	echo $this->Html->script('/bower_components/angular-classy/angular-classy.min');
	echo $this->Html->script('/bower_components/angular-resource/angular-resource.min');
	echo $this->Html->script('/bower_components/angular-sanitize/angular-sanitize.min');
	echo $this->Html->script('/bower_components/lodash/lodash.min');
	echo $this->Html->script('/bower_components/angular-google-maps/dist/angular-google-maps.min');
	echo $this->Html->css('/bower_components/isteven-angular-multiselect/isteven-multi-select');
	echo $this->Html->script('/bower_components/isteven-angular-multiselect/isteven-multi-select');
	// App
	echo $this->Html->script('Admin./js/Mensagens/app.js');
	// Controllers
	echo $this->Html->script('Admin./js/Mensagens/index.js');
?>
<div class="row" ng-app="estacaoApp" ng-controller="mensagemCtrl">

	<div class="col-md-3">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Pastas</h3>
			</div>
			<ul class="list-group">
				<li ng-class="{'active':selecteds.Folder==item.Pasta}" style="cursor: pointer" ng-click="changeFolder(item)" ng-repeat="item in pastas" class="list-group-item">{{item.Pasta.nome}}</li>
		</div>
	</div>
	
	<div class="col-md-9">
		
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">{{selecteds.Folder.nome}}</h3>
				<div class="box-tools">
					<div class="btn-group">
						<button class="btn btn-sm btn-default" ng-click="nova()">Nova Mensagem</button>
					</div>
				</div>
			</div>
			<div ng-if="mensagens.length == 0"  class="box-body">
				<h4 class="text-warning">
					Nenhuma mensagem encontrada nesta pasta!
				</h4>
			</div>
			<div style="max-height: 500px; overflow: auto;">
				<table ng-if="mensagens.length > 0" class="table table-hover">
					<thead>
						<tr>
							<th class="col-md-1">&nbsp;</th>
							<th class="col-md-1">Data</th>
							<th class="col-md-3">De</th>
							<th class="col-md-3">Para</th>
							<th>Assunto</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="item in mensagens" style="cursor: pointer;" ng-click="showMessage(item)">
							<td>
								<i class="fa fa-trash"></i>
							</td>
							<td>{{item.Mensagem.data|date:'d/MM/y H:m'}}</td>
							<td>{{item.Remetente.nome}} [{{item.Remetente.Grupo.nome}}] {{item.Remetente.Municipio.nome}}</td>
							<td>{{item.Destinatario.nome}} [{{item.Destinatario.Grupo.nome}}] {{item.Destinatario.Municipio.nome}}</td>
							<td>{{item.Mensagem.assunto}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div ng-if="selectedMessage" class="box box-default box-solid">
			<div class="box-header">
				<h3 class="box-title">{{selectedMessage.assunto}}</h3>
			</div>
			<div class="box-body" ng-bind-html="selectedMessage.body"></div>
			<div class="box-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-default" ng-click="responder()">Responder</button>
					<button class="btn btn-default" ng-click="encaminhar()">Encaminhar</button>
					<button class="btn btn-danger" ng-click="excluir()">Excluir</button>
				</div>
			</div>
			<div ng-if="escrevendo">
			<div class="box-body">
				<div     
					isteven-multi-select
					input-model="destList"
					output-model="destSelected"
					button-label="nome"
					item-label="nome grupo"
					tick-property="ticked"
					helper-elements="filter"
					translation="localLang"
				>
				</div>
				<hr>
				<input type="text" class="form-control" placeholder="Assunto" ng-model="mensagem.assunto">
				<hr>
				<textarea class="form-control" rows="5" ng-model="mensagem.body"></textarea>
			</div>
			<div class="box-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-default" ng-click="cancelar()">Cancelar</button>
					<button class="btn btn-default" ng-click="enviar()">Enviar</button>
				</div>
			</div>
			</div>
		</div>
		
		<div ng-if="escrevendoNova">
			<div class="box box-primary box-solid">
			<div class="box-body">
				<div
					isteven-multi-select
					input-model="destList"
					output-model="destSelected"
					button-label="nome"
					item-label="nome grupo"
					tick-property="ticked"
					helper-elements="filter"
					translation="localLang"
				>
				</div>
				<hr>
				<input type="text" class="form-control" placeholder="Assunto" ng-model="mensagem.assunto">
				<hr>
				<textarea class="form-control" rows="5" ng-model="mensagem.body"></textarea>
			</div>
			<div class="box-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-default" ng-click="cancelar()">Cancelar</button>
					<button class="btn btn-default" ng-click="enviar()">Enviar</button>
				</div>
			</div>
			</div>
		</div>
		
	</div>
	
</div>