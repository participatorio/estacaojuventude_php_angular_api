<div class="content body">

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Temáticas</h3>
			<div class="box-tools pull-right">
			<div class="btn-group">
				<?php echo $this->Html->link('Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-sm btn-default'));?>
			</div>
		</div>
		</div>
		<div class="box-body">
			<table class="table table-border">

				<tr>
					<th class="col-md-1">&nbsp;</th>
					<th class="col-md-11">Nome</th>
				</tr>

				<?php foreach($tematicas as $item) { ?>
				
				<tr>
					<td>
						<div class="btn-group">
							<?php echo $this->Element('index-actions', array('id'=>$item['Tematica']['id'])); ?>
						</div>
					</td>
					<td><?php echo $item['Tematica']['nome'];?></td>
				</tr>
				
				<?php if (count($item['children']) > 0) { ?>
				<tr>
					<td class="bg-gray" colspan="2" style="padding: 5px 5px 5px 25px;">
						<table class="table table-bordered table-condensed" style="margin-bottom: 0px !important;">
				<?php foreach($item['children'] as $subitem) { ?>
				
				<tr>
					<td class="col-md-1">
						<div class="btn-group">
							<?php echo $this->Element('index-actions', array('id'=>$subitem['Tematica']['id'])); ?>
						</div>
					</td>
					<td><?php echo $subitem['Tematica']['nome'];?></td>
				</tr>
				
				<?php } ?>
						</table>
					</td>
				</tr>
				
				<?php } ?>
				
				<?php } ?>

			</table>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<?php echo $this->Html->link('<i class="fa fa-plus">&nbsp;</i>Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-sm btn-primary'));?>
			</div>
		</div>
</div>
