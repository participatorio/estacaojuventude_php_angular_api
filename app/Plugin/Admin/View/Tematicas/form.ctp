<div class="content body">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Temática</h3>
		</div>
		<?php echo $this->Form->create('Tematica'); ?>
		<?php echo $this->Form->input('id'); ?>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('nome'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('parent_id', array('label'=>'Temática Pai')); ?>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-default" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
