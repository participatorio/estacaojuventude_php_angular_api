<div class="content body">
	<div class="page-header">
		<h3>Formulário Usuário</h3>
	</div>
	<div class="box box-primary">
		<?php echo $this->Form->create('Usuario'); ?>
		<?php echo $this->Form->input('id'); ?>
		<div class="box-header">
			<h3 class="box-title">Usuário</h3>
			<div class="box-tools pull-right">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<div class="box-body">

			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('login', array('label'=>'Login')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('senha', array('label'=>'Senha','type'=>'password')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('nome', array('label'=>'Nome')); ?>
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-3">
					<?php echo $this->Form->input('email', array('label'=>'Email')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('grupo_id', array('label'=>'Grupo')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('estado_id', ['label'=>'Estado']); ?>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label">Município</label>
						<input id="search_municipio" value="<?php echo (isset($this->data['Municipio']['nome']))?($this->data['Municipio']['nome']):('');?>" placeholder="Pesquise pelo Município" type="text" class="form-control">
						<br>
						<?php echo $this->Form->input('municipio_id', ['label'=>false]); ?>
					</div>
				</div>
			</div>

		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#search_municipio').keyup(function(){
			value = $(this).val();
			if (value.length >= 4) {
				$('#UsuarioMunicipioId').html('');
				$.ajax({
					url: '/admin/usuarios/municipio_ajax',
					data: {nome:value},
					method: 'post',
					dataType: 'json',
					success: function(result) {
						$('#UsuarioMunicipioId').append('<option value="">Nenhum</option>');
						result.forEach(function(item){
							$('#UsuarioMunicipioId').append('<option value="'+item.Municipio.id+'">'+item.Estado.sigla+' / '+item.Municipio.nome+'</option>');
						});
					}
				});
			}
		});
	});
</script>
