<div class="content body">
	<?php //pr($usuarios[1]); ?>
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Lista de Usuário</h3>
			<div class="box-tools pull-right">
			<div class="btn-group">
				<?php echo $this->Html->link('Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-sm btn-default'));?>
			</div>
		</div>
		</div>
		<div class="box-body">

			<table class="table table-hover">

				<tr>
					<th class="col-md-1">&nbsp;</th>
					<th class="col-md-2">Nome</th>
					<th class="col-md-2">Usuário</th>
					<th class="col-md-2">Grupo</th>
					<th class="col-md-3">Email</th>
					<th class="col-md-2">Estado/Município</th>
				</tr>

				<?php foreach($usuarios as $item) { ?>
				
				<tr>
					<td>
						<div class="btn-group">
							<?php echo $this->Element('index-actions', array('id'=>$item['Usuario']['id'])); ?>
						</div>
					</td>
					<td><?php echo $item['Usuario']['nome'];?></td>
					<td><?php echo $item['Usuario']['login'];?></td>
					<td><?php echo $item['Grupo']['nome'];?></td>
					<td><?php echo $item['Usuario']['email'];?></td>
					<td><?php echo $item['Estado']['sigla'];?> - <?php echo $item['Municipio']['nome'];?></td>
				</tr>
				
				<?php } ?>

			</table>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<?php echo $this->Element('paginator'); ?>
			</div>
		</div>
</div>
