<div class="content body">
	<div class="page-header">
		<h3 class="box-title">Lista de Órgãos Executores</h3>
		<div class="pull-right">
			<div class="btn-group">
			<?php echo $this->Html->link('Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-primary'));?>
			</div>
		</div>
	</div>
	<hr>
	<div class="box box-primary">

		<table class="table">

			<tr class="active">
				<th class="col-md-2">&nbsp;</th>
				<th>Nome</th>
				<th>Contato</th>
				<th>Email</th>
				<th class="col-md-2">Telefone</th>
			</tr>

			<?php foreach($orgaos as $item) { ?>
			
			<tr>
				<td>
					<div class="btn-group">
						<?php echo $this->Element('index-actions', array('id'=>$item['Orgao']['id'])); ?>
					</div>
				</td>
				<td><?php echo $item['Orgao']['nome'];?> (<?php echo $item['Orgao']['sigla'];?>)</td>
				<td><?php echo $item['Orgao']['contato'];?></td>
				<td><?php echo $item['Orgao']['email_contato'];?></td>
				<td><?php echo $item['Orgao']['telefone_contato'];?></td>
			</tr>
			
			<?php } ?>

		</table>
	
		<div class="box-footer">
			<div class="paginator pull-right">
				<ul class="pagination" style="padding: 0; margin: 0;">
					<?php echo $this->Paginator->first('Primeira'); ?>
					<?php echo ($this->Paginator->request->params['paging']['Orgao']['pageCount']>1)?($this->Paginator->prev('Anterior')):(''); ?>
					<?php echo $this->Paginator->numbers(); ?>
					<?php echo ($this->Paginator->request->params['paging']['Orgao']['pageCount']>1)?($this->Paginator->next('Próxima')):(''); ?>
					<?php echo $this->Paginator->last('Última'); ?>
				</ul>
			</div>
		</div>
		
	</div>

</div>