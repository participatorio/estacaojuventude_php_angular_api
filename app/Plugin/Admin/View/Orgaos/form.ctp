<div class="content body">
	
	<?php echo $this->Form->create('Orgao'); ?>
	<?php echo $this->Form->input('id'); ?>
	
	<div class="page-header">
			
			<span>Órgão Executor</span>

			<div class="box-tools pull-right">
				<div class="btn-group">
					<button class="btn btn-primary" type="submit">Salvar</button>
					<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
				</div>
			</div>
	</div>
	
	<div class="box box-primary">
		<div class="box-body">

			<div class="row">
				<div class="col-md-2">
					<?php echo $this->Form->input('sigla', array('label'=>'Sigla')); ?>
				</div>
				<div class="col-md-10">
					<?php echo $this->Form->input('nome', array('label'=>'Nome Oficial')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('contato', array('label'=>'Contato')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('email_contato', array('label'=>'Email')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('telefone_contato', array('label'=>'Telefone')); ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<?php echo $this->Form->input('endereco', array('label'=>'Endereço','type'=>'text')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('cep', array('label'=>'CEP')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('bairro', array('label'=>'Bairro')); ?>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">Estado</label>
						<input id="search_estado" placeholder="Pesquise pelo Estado" type="text" class="form-control">
						<br>
						<?php echo $this->Form->input('estado_id', ['label'=>false]); ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label">Município</label>
						<input id="search_municipio" placeholder="Pesquise pelo Município" type="text" class="form-control">
						<br>
						<?php echo $this->Form->input('municipio_id', ['label'=>false]); ?>
					</div>
				</div>
			</div>
			
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#search_estado').keyup(function(){
			value = $(this).val();
			if (value.length >= 2) {
				$('#OrgaoEstadoId').html('');
				$.ajax({
					url: '/admin/orgaos/estado_ajax',
					data: {nome:value},
					method: 'post',
					dataType: 'json',
					success: function(result) {
						html = '';
						result.forEach(function(item){
							html+='<option value="'+item.Estado.id+'">'+item.Estado.sigla+'</option>';
						});
						$('#OrgaoEstadoId').html(html);
					}
				});
			}
		});
		
		$('#search_municipio').keyup(function(){
			value = $(this).val();
			if (value.length >= 4) {
				$('#OrgaoMunicipioId').html('');
				$.ajax({
					url: '/admin/orgaos/municipio_ajax',
					data: {nome:value},
					method: 'post',
					dataType: 'json',
					success: function(result) {
						html = '';
						result.forEach(function(item){
							html+='<option value="'+item.Municipio.id+'">'+item.Municipio.nome+'</option>';
						});
						$('#OrgaoMunicipioId').html(html);
					}
				});
			}
		});
	});
</script>

