<div class="content-header page-header">
	Páginas
</div>
<div class="content body">

	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Lista de Páginas</h3>
			<div class="box-tools pull-right">
			<div class="btn-group">
				<?php echo $this->Html->link('<i class="fa fa-plus">&nbsp;</i>Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-sm btn-primary'));?>
			</div>
		</div>
		</div>
		<div class="box-body">
			
			<table class="table table-hover">

				<tr>
					<th class="col-md-2">&nbsp;</th>
					<th class="col-md-2">Slug</th>
					<th class="col-md-8">Título</th>
				</tr>

				<?php foreach($paginas as $item) { ?>
				
				<tr>
					<td>
						<div class="btn-group">
							<?php echo $this->Element('index-actions', array('id'=>$item['Pagina']['id'])); ?>
						</div>
					</td>
					<td><?php echo $item['Pagina']['slug'];?></td>
					<td><?php echo $item['Pagina']['titulo'];?></td>
				</tr>
				
				<?php } ?>

			</table>
			
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<?php echo $this->Html->link('<i class="fa fa-plus">&nbsp;</i>Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-sm btn-primary'));?>
			</div>
		</div>
</div>
