<?php
	echo $this->Html->script('/bower_components/angular/angular.min');
	echo $this->Html->script('/bower_components/angular-classy/angular-classy.min');
	echo $this->Html->script('/bower_components/angular-resource/angular-resource.min');
	echo $this->Html->script('/bower_components/angular-file-upload/dist/angular-file-upload.min');

	// App
	echo $this->Html->script('Admin./js/Paginas/app.js');
	// Controllers
	echo $this->Html->script('Admin./js/Paginas/index.js');
?>

<div class="content body" ng-app="estacaoApp" ng-controller="paginaCtrl">
	<div class="page-header">
		<span>Formulário Páginas</span>
	</div>
	<div class="box box-primary">
		<?php echo $this->Form->create('Pagina'); ?>
		<input type="hidden" id="editBack" name="data[Pagina][editar]" value="0">
		<?php echo $this->Form->input('id'); ?>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('slug'); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('titulo', array('label'=>'Título')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<?php echo $this->Form->input('conteudo', array('label'=>'Conteúdo', 'rows'=>20)); ?>
				</div>
				<div class="col-md-3">
					<label>Imagens</label>
					<input type="file" nv-file-select="" uploader="uploader" multiple="">
					<hr>
					<div class="progress" style="">
						<div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }" style="width: 0%;"></div>
					</div>
					<table class="table">
						<tr>
							<th>&nbsp;</th>
							<th>Descrição</th>
							<th>Tipo</th>
						</tr>
						<?php foreach ($imagens as $item) { ?>
						<tr>
							<td></td>
							<td><?php echo $item['Imagem']['descricao'];?></td>
							<td><?php echo $item['Imagem']['tipo'];?></td>
						</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		tinymce.init({
			selector:'textarea',
			menubar: false,
			toolbar: 'redo undo | styleselect | paste | code media | bold italic | bullist numlist', 
		});
	});
</script>
