<?php
	echo $this->Html->script('/bower_components/angular/angular.min');
	echo $this->Html->script('/bower_components/angular-classy/angular-classy.min');
	echo $this->Html->script('/bower_components/angular-resource/angular-resource.min');
	echo $this->Html->script('/bower_components/lodash/lodash.min');
	echo $this->Html->script('/bower_components/angular-google-maps/dist/angular-google-maps.min');
	// App
	echo $this->Html->script('Admin./js/Localizacoes/app.js');
	// Controllers
	echo $this->Html->script('Admin./js/Localizacoes/index.js');

	// Verfica coordenadas e zoom
	if (isset($municipio)) {
	$pos = [
		'coords' => [
			'latitude' => $municipio['latitude'],
			'longitude' => $municipio['longitude']
		],
		'zoom' => $municipio['zoom']
	];
	}

	if (isset($this->data['Localizacao'])) {

		//if ( intval( $this->data['Localizacao']['latitude'] ) != 0 ) {
			$pos['coords']['latitude'] = $this->data['Localizacao']['latitude'];
		//}
		//if ( intval( $this->data['Localizacao']['longitude'] ) != 0 ) {
			$pos['coords']['longitude'] = $this->data['Localizacao']['longitude'];
		//}
		//if ( intval( $this->data['Localizacao']['zoom'] ) != 0 ) {
			$pos['zoom'] = $this->data['Localizacao']['zoom'];
		//}

	}
	
	//pr($pos);


?>
<style>
.angular-google-map-container { height: 500px; }
</style>
<div id="LocalizacaoCtrl" ng-app="estacaoApp" ng-controller="localizacaoCtrl">
<div class="content body">
	<div class="page-header">
		Formulário Localização
	</div>
	<div class="box box-primary">
		<?php echo $this->Form->create('Localizacao', ['novalidate' => true]); ?>
		<?php echo $this->Form->input('id'); ?>
		<?php echo $this->Form->input('ocorrencia_id', ['type'=>'hidden']); ?>
		<input type="hidden" ng-model="map.center.latitude" ng-init="map.center.latitude=<?php echo $pos['coords']['latitude']; ?>">
		<input type="hidden" ng-model="map.center.longitude" ng-init="map.center.longitude=<?php echo $pos['coords']['longitude']; ?>">
		<input type="hidden" ng-model="map.zoom" ng-init="map.zoom=<?php echo $pos['zoom']; ?>">
		<div class="box-header">
			<h3 class="box-title">Localização</h3>
			<div class="box-tools pull-right">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<div class="box-body">

			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('local', array('type'=>'text','label'=>'Local')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('horario_inicio', array('type'=>'text','label'=>'Horário de Início')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('horario_fim', array('type'=>'text','label'=>'Horário de Fim')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('nome_referencia', array('label'=>'Nome de Referência')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('email', array('label'=>'Email')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('telefone', array('label'=>'Telefone')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('endereco', array('label'=>'Endereço','type'=>'text')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('horario_funcionamento', array('label'=>'Horário de Funcionamento')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('quantidade_vagas', array('label'=>'Quantidade de Vagas')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('lougradouro', array('label'=>'Logradouro')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('bairro', array('label'=>'Bairro')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('cep', array('label'=>'CEP')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('latitude', array('ng-model'=>'marker.coords.latitude', 'ng-init'=>'marker.coords.latitude='.$pos['coords']['latitude'], 'label'=>'Latitude')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('longitude', array('ng-model'=>'marker.coords.longitude', 'ng-init'=>'marker.coords.longitude='.$pos['coords']['longitude'], 'label'=>'Longitude')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('zoom', array('ng-model'=>'map.zoom','label'=>'Zoom')); ?>
				</div>
			</div>
		</div>
		<div style="min-height: 500px;">
			<ui-gmap-google-map
				center="map.center"
				zoom="map.zoom"
				options="map.options">
				<ui-gmap-marker
					coords="marker.coords"
					options="marker.options"
					idkey="marker.id">
			</ui-gmap-google-map>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
		
