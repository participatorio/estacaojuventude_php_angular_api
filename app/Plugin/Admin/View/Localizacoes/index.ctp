<div class="content body">
	<div class="page-header">
		<span>Localizações</span>
		<div class="pull-right">
			<div class="btn-group">
				<?php echo $this->Html->link('Adicionar', array('action'=>'add', $ocorrencia_id), array('escape'=>false,'class'=>'btn btn-primary'));?>
			</div>
		</div>
	</div>
	
	<div class="box box-primary">
		
		<div class="box-body">
			
			<?php if (count($localizacoes) == 0) { ?>
				<span class="text-warning">Nenhum registro encontrado!</span>
			<?php } else { ?>

			<table class="table table-hover">

				<tr>
					<th class="col-md-2">&nbsp;</th>
					<th class="col-md-8">Local</th>
					<th class="col-md-2">Telefone</th>
					
				</tr>

				<?php foreach($localizacoes as $item) { ?>
				
				<tr>
					<td>
						<div class="btn-group">
							<?php echo $this->Element('index-actions', array('id'=>$item['Localizacao']['id'],'ocorrencia_id'=>$item['Localizacao']['ocorrencia_id'])); ?>
						</div>
					</td>
					<td><?php echo $item['Localizacao']['local'];?></td>
					<td><?php echo $item['Localizacao']['telefone'];?></td>
				</tr>
				
				<?php } ?>

			</table>
			
			<?php } ?>
		</div>
	</div>	
</div>
