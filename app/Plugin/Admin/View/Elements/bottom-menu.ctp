<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
	<div class="container-fluid">
		<a class="navbar-brand pull-right" id="timerClock" href="#">
			15:00
		</a>
		<ul class="nav navbar-nav pull-right">
			<li><?php echo $this->Html->link('Sair','/logout'); ?></li>
		</ul>
	</div>
</nav>
<script>
	startTimer = 15;
	min = 0;
	sec = 15;
	$(document).ready(function(){

		reset(); // Inicia timer
		window.setInterval('timerStep()', 1000);

	});
	function reset() {
		min = startTimer;
		sec = 0;
	}
	function timerStep() {
		
		sec--;
		if (sec < 0) {
			sec = 59;
			min--;
		}
		if (min < 0) {
			location.href = ('/logout');
		} else {
			showTimer();
		}
	}
	function showTimer() {
		if (sec < 10) strSec = '0'+sec; else strSec = sec;
		if (min < 10) strMin = '0'+min; else strMin = min;
		$('#timerClock').html(strMin+':'+strSec);
	}

</script>
