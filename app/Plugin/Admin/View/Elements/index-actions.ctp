<?php if (isset($ocorrencia_id)) $ocor = $ocorrencia_id; else $ocor = 0; ?>
<?php echo $this->Html->link('<i class="fa fa-pencil fa-fw"></i>', array('action'=>'edit', $id), array('escape'=>false,'class'=>'btn btn-default',"data-toggle"=>"tooltip", "title"=>"Editar"));?>
<?php echo $this->Form->postLink('<i class="fa fa-trash fa=fw text-danger">&nbsp;</i>', array('action'=>'del', $id, $ocor), array('confirm'=>'Tem Certeza?','escape'=>false,'class'=>'btn btn-default',"data-toggle"=>"tooltip", "title"=>"Excluir"));?>
