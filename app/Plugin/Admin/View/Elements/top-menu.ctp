<nav class="navbar navbar-static-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
			
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
	    <li><?php echo $this->Html->link('Home', array('plugin'=>'admin','controller'=>'home')); ?></li>
        <li><?php echo $this->Html->link('Programas',array('controller'=>'programas','action'=>'index')); ?></li>
        <li><?php echo $this->Html->link('Ocorrências',array('controller'=>'ocorrencias','action'=>'index')); ?></li>
        <li><?php echo $this->Html->link('Órgãos Executores',array('controller'=>'orgaos','action'=>'index')); ?></li>
      </ul>


      <ul class="nav navbar-nav navbar-right">
        <?php if ($user['Grupo']['admin'] == true) { ?>
       
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Geral <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><?php echo $this->Html->link('Estados',array('controller'=>'estados','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Municípios',array('controller'=>'municipios','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Níveis',array('controller'=>'niveis','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Situações',array('controller'=>'situacoes','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Status',array('controller'=>'status','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Temáticas',array('controller'=>'tematicas','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Temporalidade',array('controller'=>'temporalidades','action'=>'index')); ?></li>
          </ul>
        </li>
        <?php } ?>
    		<li><?php echo $this->Html->link('Mensagens',array('controller'=>'mensagens','action'=>'index')); ?></li>
    		<?php /*<li><?php echo $this->Html->link('Jovens',array('controller'=>'jovens','action'=>'index')); ?></li> */?>
        <?php if ($user['Grupo']['admin'] == true) { ?>
    		<li><?php echo $this->Html->link('Páginas',array('controller'=>'paginas','action'=>'index')); ?></li>
    		<li><?php echo $this->Html->link('Usuários',array('controller'=>'usuarios','action'=>'index')); ?></li>
        <?php } ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $user['Usuario']['nome'];?><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><?php echo $this->Html->link('Profile','/profile'); ?></li>
            <li><?php echo $this->Html->link('Logout','/logout'); ?></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
