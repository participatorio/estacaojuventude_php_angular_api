<div class="content body">
	
	<div class="page-header">
		
			<h3>Temática de Programas</h3>
			<small><?php echo $programa['Programa']['nome_oficial']; ?></small>
			<hr>
			<div class="btn-group">
				<?php echo $this->Html->link('Programa', ['action'=>'edit', $programa['Programa']['id']],['class'=>'btn btn-default']); ?>
				<?php echo $this->Html->link('Temáticas', ['action'=>'tematicas', $programa['Programa']['id']],['class'=>'btn btn-default']); ?>
				<?php echo $this->Html->link('Órgãos Executores', ['action'=>'orgaos', $programa['Programa']['id']],['class'=>'btn btn-default']); ?>
			</div>
			
			<div class="box-tools pull-right">
				<div class="btn-group">
					<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
				</div>
			</div>

			
	</div>
	
	<div class="row">
		<?php echo $this->Form->create('ProgramaTematica'); ?>
		<div class="col-md-5">
			<select id="ProgramaTematicaTematicasList" class="form-control" multiple="multiple" style="height: 300px">
				<?php foreach($tematicas as $item) { ?>
				<option value="<?php echo $item['Tematica']['id'];?>"><?php echo $item['Tematica']['nome'];?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-2">
			
			<div class="btn-group-vertical btn-block" style="margin-top: 50px;">
				<button id="ProgramaTematicaAdd" type="button" class="btn btn-default">
					<i class="fa fa-chevron-right"></i>
				</button>
				<br>
				<button id="ProgramaTematicaDel" type="button" class="btn btn-default">
					<i class="fa fa-chevron-left"></i>
				</button>
				<br>
				<button id="ProgramaTematicaSubmit" type="submit" class="btn btn-default">
					<i class="fa fa-save"></i>
				</button>
				<br>
				<button type="button" class="btn btn-default">
					<i class="fa fa-close"></i>
				</button>
			</div>
		</div>
		<div class="col-md-5">
			<select id="ProgramaTematicaTematicaId" name="data[ProgramaTematica][tematica_id][]" class="form-control" multiple="multiple" style="height: 300px">
				<?php foreach($programas_tematicas as $item) { ?>
				<option value="<?php echo $item['Tematica']['id'];?>"><?php echo $item['Tematica']['nome'];?></option>
				<?php } ?>
			</select>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#ProgramaTematicaSubmit').click(function(){
			selectBox = document.getElementById("ProgramaTematicaTematicaId");
			for (var i = 0; i < selectBox.options.length; i++) { 
             selectBox.options[i].selected = true; 
			} 
		});
		
		$('#ProgramaTematicaAdd').click(function() {
			options = $('#ProgramaTematicaTematicasList option:selected');
			$(options).each(function(item){
				$('#ProgramaTematicaTematicaId').append(options[item]);
			});
		});
		
		$('#ProgramaTematicaDel').click(function() {
			options = $('#ProgramaTematicaTematicaId option:selected');
			$(options).each(function(item){
				$('#ProgramaTematicaTematicasList').append(options[item]);
			});
		});
	});
</script>

	