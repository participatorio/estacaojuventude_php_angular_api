<div class="content body">
	
	<?php if ($user['Grupo']['id'] != 4) { ?>
	
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-3">
			<?php if ($user['Grupo']['id'] == 1 && $this->data['Programa']['status_id'] == 1) {
				$validateDisabled = 'disabled';
			} else if ($user['Grupo']['id'] == 2 && $this->data['Programa']['status_id'] == 3) {
				$validateDisabled = 'disabled';
			} else {
				$validateDisabled = false;
			} ?>
			<?php echo $this->Html->link('<h4>Validar</h4>', ['action'=>'doValidate',$this->data['Programa']['id']],['disabled'=>$validateDisabled,'escape'=>false,'class'=>'btn btn-success btn-block']); ?>
		</div>
		<div class="col-md-3">
			<?php if ($this->data['Programa']['status_id'] == 4) {
				$rejectDisabled = 'disabled';
			} else {
				$rejectDisabled = false;
			} ?>
			<?php echo $this->Html->link('<h4>Rejeitar</h4>', ['action'=>'doReject',$this->data['Programa']['id']],['disabled'=>$rejectDisabled, 'escape'=>false,'class'=>'btn btn-danger btn-block']); ?>
		</div>
	</div>
	<hr>
	<?php } ?>
	<style>
		.box .row {
			border-bottom: 1px dashed #dedede;
			margin: 5px;
		}
	</style>
	<div class="box box-default">
		<div class="box-header">
			<div class="btn-group pull-right">
				<?php echo $this->Html->link('Voltar', ['action'=>'index'], ['class'=>'btn btn-default']); ?>
			</div>
		</div>

		<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<label>Sigla</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['sigla']; ?></p>
			</div>
			<div class="col-md-3">
				<label>Nome Oficial</label>
				<h4 class="form-control-static"><?php echo $this->data['Programa']['nome_oficial']; ?></h4>
			</div>
			<div class="col-md-3">
				<label>Nome de Divulgação</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['nome_divulgacao']; ?></p>
			</div>
			<div class="col-md-3">
				<label>Lei de Criação</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['lei_criacao']; ?></p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<label>Data de Início</label>
				<p class="form-control-static"><?php echo $this->Time->format('d/m/Y', $this->data['Programa']['data_inicio']); ?></p>
			</div>
			<div class="col-md-3">
				<label>Nível</label>
				<p class="form-control-static"><?php echo $this->data['Nivel']['nome']; ?></p>
			</div>
			<div class="col-md-3">
				<label>Status</label>
				<p class="form-control-static <?php echo ($this->data['Programa']['status_id']==2)?('text-warning'):('text-success');?>"><?php echo $this->data['Status']['nome']; ?></p>
			</div>
			<div class="col-md-3">
				<label>Situação</label>
				<p class="form-control-static <?php echo ($this->data['Programa']['situacao_id']==2)?('text-danger'):('text-success');?>"><?php echo $this->data['Situacao']['nome']; ?></p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<label>Temporalidade</label>
				<p class="form-control-static"><?php echo $this->data['Temporalidade']['nome']; ?></p>
			</div>
			<div class="col-md-3">
				<label>Duração</label>
				<h4 class="form-control-static"><?php echo $this->data['Programa']['duracao']; ?></h4>
			</div>
			<div class="col-md-3">
				<label>Idade Mínima</label>
				<h4 class="form-control-static"><?php echo $this->data['Programa']['idade_minima']; ?></h4>
			</div>
			<div class="col-md-3">
				<label>Idade Máxima</label>
				<h4 class="form-control-static"><?php echo $this->data['Programa']['idade_maxima']; ?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Descrição</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['descricao']; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Benefícios</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['beneficios']; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Critérios de Acesso</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['criterios_acesso']; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Público Alvo</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['publico_alvo']; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Objetivos</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['objetivos']; ?></p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<label>Parceiros</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['parceiros']; ?></p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<label>Observação</label>
				<p class="form-control-static"><?php echo $this->data['Programa']['observacao']; ?></p>
			</div>
		</div>
	</div>
</div>