<div class="content body">
	<div class="page-header">
		<div class="row">
			<div class="col-md-10">
				<h3>Listar Programas</h3>
			</div>
			<div class="col-md-2">
				<div class="btn-group pull-right">
					<?php echo $this->Html->link('Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-primary'));?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10">
	
			<div class="box box-primary">
				
				<?php if (count($programas) == 0) { ?>
				<div class="box-body">
					<span class="text-danger">Nenhum Programa encontrado!</span>
				</div>
				<?php } else { ?>
		
					<table class="table">
		
						<tr class="active">
							<th class="col-md-3">&nbsp;</th>
							<th class="col-md-2">Nome Oficial</th>
							<th class="col-md-1">Nível</th>
							<th class="col-md-2">Data Início</th>
							<th class="col-md-2">Status</th>
							<th class="col-md-2">Situação</th>
							
						</tr>
		
						<?php foreach($programas as $item) { ?>
						
						<tr>
							<td>
								<div class="btn-group">
									<?php echo $this->Element('index-actions', array('id'=>$item['Programa']['id'])); ?>
									<?php if ($user['Grupo']['id'] < 3) { ?>
									<?php echo $this->Html->link('<i class="fa fa-fw fa-eye"></i>', ['action'=>'view', $item['Programa']['id']], ['escape'=>false, 'class'=>'btn btn-default']); ?>
									<?php } ?>
								</div>
							</td>
							<td><?php echo $item['Programa']['nome_oficial'];?></td>
							<td><?php echo $item['Nivel']['nome'];?></td>
							<td><?php echo $item['Programa']['data_inicio'];?></td>
							<td><?php echo $item['Status']['nome'];?></td>
							<td>
								<div class="btn-group">
									<?php if (count($item['ProgramasTematicas']) > 0 && count($item['OrgaosProgramas']) > 0) { ?>
									<a href="/admin/programas/ativa_desativa/<?php echo $item['Programa']['id'].'/'.$item['Programa']['situacao_id'];?>" title="<?php echo ($item['Programa']['situacao_id']==2)?('Clique para Ativar'):('Clique para Desativar');?>" data-toggle="tooltip" class="btn <?php echo ($item['Programa']['situacao_id']==2)?('btn-default'):('btn-success');?>">
										<i class="fa fa-fw fa-power-off"></i>
									</a>
									<?php } else { ?>
									<span class="text-warning">Não é possível Ativar um Programa sem ao menos uma Temática e um Órgão Executor.</span>
									<?php } ?>
								</div>
							</td>
						</tr>
						
						<?php } ?>
		
					</table>
				
				
				
				<div class="box-footer">
					<div class="paginator pull-right">
						<ul class="pagination" style="padding: 0; margin: 0;">
							<?php echo $this->Paginator->first('Primeira'); ?>
							<?php echo ($this->Paginator->request->params['paging']['Programa']['pageCount']>1)?($this->Paginator->prev('Anterior')):(''); ?>
							<?php echo $this->Paginator->numbers(); ?>
							<?php echo ($this->Paginator->request->params['paging']['Programa']['pageCount']>1)?($this->Paginator->next('Próxima')):(''); ?>
							<?php echo $this->Paginator->last('Última'); ?>
						</ul>
					</div>
				</div>
				
				<?php } ?>
				
			</div>
		</div>
		<div class="col-md-2">
			<div class="box box-success <?php if($this->data) { ?>box-solid<?php } ?>">
				
				<div class="box-header with-border">
					<h3 class="box-title">Filtros</h3>
				</div>
				<?php echo $this->Form->create('search'); ?>
				<div class="box-body">
					<?php echo $this->Form->input('nome'); ?>
					<?php echo $this->Form->input('status_id'); ?>
					<?php echo $this->Form->input('situacao_id'); ?>
				</div>
				<div class="box-footer">
					<div class="btn-group">
					<?php echo $this->Form->submit('Ok'); ?>
					<?php echo $this->Html->link('Zerar', ['action'=>'search_reset'], ['class'=>'btn btn-default']); ?>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
			
		</div>
	</div>
</div>
