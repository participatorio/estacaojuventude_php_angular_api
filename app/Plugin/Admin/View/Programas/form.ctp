
<div class="content body">
	
	<?php echo $this->Form->create('Programa',['novalidate' => true]); ?>
	<?php echo $this->Form->input('id'); ?>

	<div class="page-header">
		
			<?php if ($this->action == 'edit') { ?>
			<div class="btn-group">
				<?php echo $this->Html->link('Programa', ['action'=>'edit', $this->data['Programa']['id']],['class'=>'btn btn-default']); ?>
				<?php echo $this->Html->link('Temáticas', ['action'=>'tematicas', $this->data['Programa']['id']],['class'=>'btn btn-default']); ?>
				<?php echo $this->Html->link('Órgãos Executores', ['action'=>'orgaos', $this->data['Programa']['id']],['class'=>'btn btn-default']); ?>
			</div>
			<?php } else { ?>
			<span>Adicionar Programa</span>
			<?php } ?>
			<div class="box-tools pull-right">
				<div class="btn-group">
					<button class="btn btn-primary" type="submit">Salvar</button>
					<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
				</div>
			</div>
	</div>

	<div class="box box-primary">
		
		<div class="box-body">

			<div class="row">
				<div class="col-md-3">
					<?php echo $this->Form->input('nome_oficial', array('label'=>'Nome Oficial')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('nome_divulgacao', array('label'=>'Nome de Divulgação')); ?>
				</div>
				<div class="col-md-1">
					<?php echo $this->Form->input('sigla', array('label'=>'Sigla')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('duracao', array('label'=>'Duração','type'=>'text')); ?>
				</div>
				<div class="col-md-3">
					<?php echo $this->Form->input('temporalidade_id', array('label'=>'Temporalidade')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('nivel_id', array('label'=>'Nível')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('lei_criacao', array('label'=>'Lei de Criação')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('data_inicio', array('type'=>'text','class'=>'brDate','label'=>'Data de Início')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('idade_minima', array('label'=>'Idade Mínima')); ?>
				</div>
				<div class="col-md-2">
					<?php echo $this->Form->input('idade_maxima', array('label'=>'Idade Máxima')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?php echo $this->Form->input('descricao', array('label'=>'Descrição')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('publico_alvo', array('label'=>'Público Alvo')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('criterios_acesso', array('label'=>'Critérios de Acesso')); ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('objetivos', array('label'=>'Objetivos')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('beneficios', array('label'=>'Benefícios')); ?>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('parceiros', array('label'=>'Parceiros')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('observacao', array('label'=>'Observação')); ?>
				</div>
			</div>
		</div>
		
		<?php echo $this->Form->end(); ?>
	</div>
	
</div>
<script>
	$(document).ready(function(){
		tinymce.init({
			selector:'textarea',
			menubar: false, 
			toolbar: 'undo redo | styleselect | bullist numlist | paste | bold italic | link image'
		});
		$('input.brDate').datepicker({
			format: "dd/mm/yyyy",
			language: "pt-BR",
			autoclose: true,
			todayHighlight: true
		});
	});
</script>
