<div class="content body">
	<?php if ($user['Grupo']['id'] != 4) { ?>
	
	<div class="row">
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-3">
			<?php if ($user['Grupo']['id'] == 1 && $this->data['Ocorrencia']['status_id'] == 1) {
				$validateDisabled = 'disabled';
			} else if ($user['Grupo']['id'] == 2 && $this->data['Ocorrencia']['status_id'] == 3) {
				$validateDisabled = 'disabled';
			} else {
				$validateDisabled = false;
			} ?>
			<?php echo $this->Html->link('<h4>Validar</h4>', ['action'=>'doValidate',$this->data['Ocorrencia']['id']],['disabled'=>$validateDisabled,'escape'=>false,'class'=>'btn btn-success btn-block']); ?>
		</div>
		<div class="col-md-3">
			<?php if ($this->data['Ocorrencia']['status_id'] == 4) {
				$rejectDisabled = 'disabled';
			} else {
				$rejectDisabled = false;
			} ?>
			<?php echo $this->Html->link('<h4>Rejeitar</h4>', ['action'=>'doReject',$this->data['Ocorrencia']['id']],['disabled'=>$rejectDisabled, 'escape'=>false,'class'=>'btn btn-danger btn-block']); ?>
		</div>
	</div>
	<hr>
	<?php } ?>
	<style>
		.box .row {
			border-bottom: 1px dashed #dedede;
			margin: 5px;
		}
	</style>
	<hr>
	<div class="box box-default">
		<div class="box-body">
			<div class="row">
				<div class="col-md-4">
					<label>Início das Inscrições</label>
					<p class="form-control-static"><?php echo $this->Time->format('d/m/Y', $this->data['Ocorrencia']['inicio_inscricoes']); ?></p>
				</div>
				<div class="col-md-4">
					<label>Fim das Inscrições</label>
					<p class="form-control-static"><?php echo $this->Time->format('d/m/Y', $this->data['Ocorrencia']['fim_inscricoes']); ?></p>
				</div>
				<div class="col-md-4">
					<label>Quantidade de Vagas</label>
					<p class="form-control-static"><?php echo $this->data['Ocorrencia']['quantidade_vagas']; ?></p>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<label>Programa</label>
					<p class="form-control-static"><?php echo $this->data['Programa']['nome_oficial']; ?></p>
				</div>
				<div class="col-md-6">
					<label>Município</label>
					<p class="form-control-static"><?php echo $this->data['Municipio']['nome'].' / '.$this->data['Municipio']['Estado']['sigla']; ?></p>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<label>Benefícios Locais</label>
					<p class="form-control-static"><?php echo $this->data['Ocorrencia']['beneficios_locais']; ?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label>Como Acessar</label>
					<p class="form-control-static"><?php echo $this->data['Ocorrencia']['como_acessar']; ?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label>Observação</label>
					<p class="form-control-static"><?php echo $this->data['Ocorrencia']['observacao']; ?></p>
				</div>
			</div>
		</div>
	</div>
</div>