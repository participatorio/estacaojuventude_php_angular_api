<?php
	if (empty($this->data)) {
		$this->request->data['Programa']['nome_oficial'] = '';
		$this->request->data['Municipio']['nome'] = '';
	}	
?>
<div class="content body">
	
	<?php echo $this->Form->create('Ocorrencia',['novalidate' => true]); ?>
	<?php echo $this->Form->input('id'); ?>
	
	<div class="page-header">
		
			<?php if ($this->action == 'edit') { ?>
			<div class="btn-group">
				<?php echo $this->Html->link('Ocorrência', ['action'=>'edit', $this->data['Ocorrencia']['id']],['class'=>'btn btn-default']); ?>
				<?php echo $this->Html->link('Órgãos Executores', ['action'=>'orgaos', $this->data['Ocorrencia']['id']],['class'=>'btn btn-default']); ?>
			</div>
			<?php } else { ?>
			<span>Adicionar Órgão Executor</span>
			<?php } ?>
			<div class="box-tools pull-right">
				<div class="btn-group">
					<button class="btn btn-primary" type="submit">Salvar</button>
					<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
				</div>
			</div>
	</div>
	
	<div class="box box-primary">
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Programa</label>
						<input id="search_programa" placeholder="Pesquise pelo Programa" type="text" class="form-control">
						<br>
						<?php echo $this->Form->input('programa_id', ['label'=>false]); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Município</label>
						<input id="search_municipio" placeholder="Pesquise pelo Município" type="text" class="form-control">
						<br>
						<?php echo $this->Form->input('municipio_id', ['label'=>false]); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('inicio_inscricoes', array('type'=>'text','class'=>'brDate','label'=>'Início das Inscrições')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('fim_inscricoes', array('type'=>'text','class'=>'brDate','label'=>'Fim das Inscrições')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('quantidade_vagas', array('label'=>'Vagas')); ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('como_acessar', array('label'=>'Como Acessar ?')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('beneficios_locais', array('label'=>'Benefícios Locais')); ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<?php echo $this->Form->input('observacao', array('label'=>'Observação')); ?>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
<script>
	$(document).ready(function(){
		$('#search_programa').keyup(function(){
			value = $(this).val();
			if (value.length >= 4) {
				$('#OcorrenciaProgramaId').html('');
				$.ajax({
					url: '/admin/ocorrencias/programa_ajax',
					data: {nome:value},
					method: 'post',
					dataType: 'json',
					success: function(result) {
						html = '';
						result.forEach(function(item){
							html+='<option value="'+item.Programa.id+'">'+item.Programa.nome_divulgacao+'</option>';
						});
						$('#OcorrenciaProgramaId').html(html);
					}
				});
			}
		});
		$('#search_municipio').keyup(function(){
			value = $(this).val();
			if (value.length >= 4) {
				$('#OcorrenciaMunicipioId').html('');
				$.ajax({
					url: '/admin/ocorrencias/municipio_ajax',
					data: {nome:value},
					method: 'post',
					dataType: 'json',
					success: function(result) {
						html = '';
						result.forEach(function(item){
							html+='<option value="'+item.Municipio.id+'">'+item.Municipio.nome+'</option>';
						});
						$('#OcorrenciaMunicipioId').html(html);
					}
				});
			}
		});
		tinymce.init({
			selector:'textarea',
			menubar: false, 
			toolbar: 'undo redo | styleselect | paste | bold italic | bullist numlist | link image'
		});
		$('input.brDate').datepicker({
			format: "dd/mm/yyyy",
			language: "pt-BR",
			autoclose: true,
			todayHighlight: true
		});
	});
</script>
