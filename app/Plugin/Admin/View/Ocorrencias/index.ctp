<div class="content body">
	<div class="page-header">
		<span>Ocorrências Municipais</span>
		<div class="pull-right">
			<div class="btn-group">
				<?php echo $this->Html->link('Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-primary'));?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10">
			<div class="box box-primary">
				
				<?php if (count($ocorrencias) == 0) { ?>
				<div class="box-body">
					<span class="text-danger">Nenhuma Ocorrência encontrada!</span>
				</div>
				<?php } else { ?>
				
					<table class="table">

						<tr class="active">
							<th class="col-md-3">&nbsp;</th>
							<th class="col-md-3">Programa</th>
							<th class="col-md-2">Município</th>
							<th class="col-md-1">Início</th>
							<th class="col-md-1">Fim</th>
							<th class="col-md-1">Status</th>
							<th class="col-md-1">Situação</th>
						</tr>

						<?php foreach($ocorrencias as $item) {
							$numLocalizacoes = count($item['Localizacao']);
							if ($numLocalizacoes == 0) {
								$textLocalizacao = 'Nenhuma Localização';
							} elseif ($numLocalizacoes == 1) {
								$textLocalizacao = count($item['Localizacao']).' Localização';
							} else {
								$textLocalizacao = count($item['Localizacao']).' Localizações';
							}
							$corLocalizacao = ($numLocalizacoes==0)?('text-red'):('text-light-blue');
						?>

						<tr>
							<td>
								<div class="btn-group">
									<?php echo $this->Element('index-actions', array('id'=>$item['Ocorrencia']['id'])); ?>

									<a href="/admin/localizacoes/index/<?php echo $item['Ocorrencia']['id'];?>" data-toggle="tooltip" title="<?php echo $textLocalizacao;?>" class="btn btn-default">
											<i class="fa fa-fw fa-map-marker <?php echo $corLocalizacao; ?>"></i>
									</a>

									<a href="/admin/ocorrencias/view/<?php echo $item['Ocorrencia']['id'];?>" class="btn btn-default" data-toggle="tooltip" title="Validação">
										<i class="fa fa-fw fa-eye"></i>
									</a>
									
								</div>
							</td>
							<td><?php echo $item['Programa']['nome_divulgacao'];?></td>
							<td><?php echo (isset($item['Municipio']['Estado']))?($item['Municipio']['nome'].' - '.$item['Municipio']['Estado']['sigla']):('Não preenchido');?></td>
							<td><?php echo $item['Ocorrencia']['inicio_inscricoes'];?></td>
							<td><?php echo $item['Ocorrencia']['fim_inscricoes'];?></td>
							<td><?php echo $item['Status']['nome'];?></td>
							<td>
								<div class="btn-group">
									<?php if (count($item['Localizacao']) > 0 ) { ?>
									<a href="/admin/ocorrencias/ativa_desativa/<?php echo $item['Ocorrencia']['id'].'/'.$item['Ocorrencia']['situacao_id'];?>" title="<?php echo ($item['Ocorrencia']['situacao_id']==2)?('Clique para Ativar'):('Clique para Desativar');?>" data-toggle="tooltip" class="btn <?php echo ($item['Ocorrencia']['situacao_id']==2)?('btn-default'):('btn-success');?>">
										<i class="fa fa-fw fa-power-off"></i>
									</a>
									<?php } else { ?>
									<span class="text-warning">Necessário uma Localização.</span>
									<?php } ?>
								
								</div>
							</td>
						</tr>

						<?php } ?>

					</table>
					
					<?php } ?>
				
				<div class="box-footer">
					<div class="paginator pull-right">
						<ul class="pagination" style="padding: 0; margin: 0;">
							<?php echo $this->Paginator->first('Primeira'); ?>
							<?php echo ($this->Paginator->request->params['paging']['Ocorrencia']['pageCount']>1)?($this->Paginator->prev('Anterior')):(''); ?>
							<?php echo $this->Paginator->numbers(); ?>
							<?php echo ($this->Paginator->request->params['paging']['Ocorrencia']['pageCount']>1)?($this->Paginator->next('Próxima')):(''); ?>
							<?php echo $this->Paginator->last('Última'); ?>
						</ul>
					</div>
				</div>
			</div>
	  	</div>
		<div class="col-md-2">
			
			<div class="box box-success <?php if($this->data) { ?>box-solid<?php } ?>">
				
				<div class="box-header with-border">
					<h3 class="box-title">Filtros</h3>
				</div>
				<?php echo $this->Form->create('search'); ?>
				<div class="box-body">
					<?php echo $this->Form->input('nome'); ?>
					<?php echo $this->Form->input('status_id'); ?>
					<?php echo $this->Form->input('situacao_id'); ?>
				</div>
				<div class="box-footer">
					<div class="btn-group">
					<?php echo $this->Form->submit('Ok'); ?>
					<?php echo $this->Html->link('Zerar', ['action'=>'search_reset'], ['class'=>'btn btn-default']); ?>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>

</div>
