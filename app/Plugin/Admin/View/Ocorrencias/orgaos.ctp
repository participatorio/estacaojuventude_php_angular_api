<div class="content body">
	
	<div class="page-header">
			
				<h3>Órgãos Executores de Ocorrências Municipais</h3>
			<small></small>
			<hr>
			<div class="btn-group">
				<?php echo $this->Html->link('Ocorrência', ['action'=>'edit', $ocorrencia['Ocorrencia']['id']],['class'=>'btn btn-default']); ?>
				<?php echo $this->Html->link('Órgãos Executores', ['action'=>'orgaos', $ocorrencia['Ocorrencia']['id']],['class'=>'btn btn-default']); ?>
			</div>
			
			<div class="box-tools pull-right">
				<div class="btn-group">
					<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
				</div>
			</div>
			
	</div>
	
	<div class="row">
		<?php echo $this->Form->create('ProgramaTematica'); ?>
		<div class="col-md-5">
			<select id="OcorrenciaOrgaoOrgaosList" class="form-control" multiple="multiple" style="height: 300px">
				<?php foreach($orgaos as $item) { ?>
				<option value="<?php echo $item['Orgao']['id'];?>"><?php echo $item['Orgao']['nome'];?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-2">
			
			<div class="btn-group-vertical btn-block" style="margin-top: 50px;">
				<button id="OcorrenciaOrgaoAdd" type="button" class="btn btn-default">
					<i class="fa fa-chevron-right"></i>
				</button>
				<br>
				<button id="OcorrenciaOrgaoDel" type="button" class="btn btn-default">
					<i class="fa fa-chevron-left"></i>
				</button>
				<br>
				<button id="OcorrenciaOrgaoSubmit" type="submit" class="btn btn-default">
					<i class="fa fa-save"></i>
				</button>
				<br>
				<button type="button" class="btn btn-default">
					<i class="fa fa-close"></i>
				</button>
			</div>
		</div>
		<div class="col-md-5">
			<select id="OcorrenciaOrgaoOrgaoId" name="data[OcorrenciaOrgao][orgao_id][]" class="form-control" multiple="multiple" style="height: 300px">
				<?php foreach($orgaos_ocorrencias as $item) { ?>
				<option value="<?php echo $item['Orgao']['id'];?>"><?php echo $item['Orgao']['nome'];?></option>
				<?php } ?>
			</select>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#OcorrenciaOrgaoSubmit').click(function(){
			selectBox = document.getElementById("OcorrenciaOrgaoOrgaoId");
			for (var i = 0; i < selectBox.options.length; i++) { 
             selectBox.options[i].selected = true; 
			} 
		});
		
		$('#OcorrenciaOrgaoAdd').click(function() {
			options = $('#OcorrenciaOrgaoOrgaosList option:selected');
			$(options).each(function(item){
				$('#OcorrenciaOrgaoOrgaoId').append(options[item]);
			});
		});
		
		$('#OcorrenciaOrgaoDel').click(function() {
			options = $('#OcorrenciaOrgaoOrgaoId option:selected');
			$(options).each(function(item){
				$('#OcorrenciaOrgaoOrgaosList').append(options[item]);
			});
		});
	});
</script>

	