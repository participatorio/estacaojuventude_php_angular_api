<div class="content-header">
	<h3>Estação Juventude</h3>
</div>
<div class="content body">
	<div class="row">
		<div class="col-md-6">
			
			<div class="box box-primary" style="min-height: 250px;">
				<div class="box-header">
					<h3 class="box-title">Mensagens</h3>
				</div>
				<div class="box-body" style="max-height: 300px; overflow: auto;">
					<?php if (count($mensagens) > 0) { ?>
					<table class="table">
						<thead>
							<tr>
								<th class="col-md-2">Data</th>
								<th class="col-md-3">De</th>
								<th>Assunto</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($mensagens as $item) { ?>
							<tr>
								<td><?php echo $item['Mensagem']['data'];?></td>
								<td><?php echo $item['Remetente']['nome'];?></td>
								<td><?php echo $item['Mensagem']['assunto'];?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<?php } else { ?>
					<div class="text-warning">
						<h4>Não há mensagens!</h4>
					</div>
					<?php } ?>
					<div class="box-footer clearfix">
						<div class="btn-group pull-right">
							<a class="btn btn-default" href="/admin/mensagens">Mensagens</a>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
		<?php if ($user['Grupo']['id'] != 3) { ?>
		<div class="col-md-6">
			
			<div class="box box-success">
				<div class="box-header">
					<h3 class="box-title">Por Validar</h3>
				</div>
				<div class="box-body" style="max-height: 300px; overflow: auto;">
					<table class="table">
						<?php if (count($ocorrencias_validar) > 0) { ?>
						<thead>
							<tr class="active">
								<th colspan="2">Ocorrências</th>
							</tr>
							<tr class="active">
								<th>Nome</th>
								<th class="col-md-4">Município</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($ocorrencias_validar as $item) { ?>
							<tr>
								<td><a href="/admin/ocorrencias/view/<?php echo $item['Ocorrencia']['id'];?>"><?php echo $item['Programa']['nome_oficial'];?></a></td>
								<td>
									<?php if (!$item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										Nacional
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['Estado']['nome'];?>
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and $item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['nome'].'/'.$item['Municipio']['Estado']['sigla'];?>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<?php } else { ?>
						<thead>
							<tr>
								<th colspan="2">
									<h4 class="text-warning">Não há Ocorrências.</h4>
								</th>
							</tr>
						</thead>
						<?php } ?>
						<?php if (count($programas_validar) > 0) { ?>
						<thead>
							<tr class="active">
								<th colspan="2">Programas</th>
							</tr>
							<tr class="active">
								<th>Nome</th>
								<th class="col-md-4">Município</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($programas_validar as $item) { ?>
							<tr>
								<td>
									<a href="/admin/programas/view/<?php echo $item['Programa']['id'];?>">
										<?php echo $item['Programa']['nome_oficial'];?>
									</a>
								</td>
								<td>
									<?php if (!$item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										Nacional
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['Estado']['nome'];?>
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and $item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['nome'].'/'.$item['Municipio']['Estado']['sigla'];?>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<?php } else { ?>
						<thead>
							<tr>
								<th colspan="2">
									<h4 class="text-warning">Não há Programas.</h4>
								</th>
							</tr>
						</thead>
						<?php } ?>
					</table>
				</div>
			</div>

		</div>
		<?php } ?>
		<div class="col-md-6">
			
			<div class="box box-warning">
				<div class="box-header">
					<h3 class="box-title">Por Vencer</h3>
				</div>
				<div class="box-body" style="max-height: 300px; overflow: auto;">
					<?php if (count($ocorrencias_vencer) > 0) { ?>
					<table class="table">
						<thead>
							<tr class="active">
								<th colspan="3">Ocorrências</th>
							</tr>
							<tr class="active">
								<th>Nome</th>
								<th class="col-md-3">Município</th>
								<th class="col-md-2">Vencimento</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($ocorrencias_vencer as $item) { ?>
							<tr>
								<td><a href="/admin/ocorrencias/edit/<?php echo $item['Ocorrencia']['id'];?>"><?php echo $item['Programa']['nome_oficial'];?></a></td>
								<td>
									<?php if (!$item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										Nacional
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['Estado']['nome'];?>
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and $item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['nome'].'/'.$item['Municipio']['Estado']['sigla'];?>
									<?php } ?>
								</td>
								<td><?php echo $item['Ocorrencia']['fim_inscricoes'];?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<?php } else { ?>
					<div class="text-warning">
						<h4>Não há registros!</h4>
					</div>
					<?php } ?>
				</div>
			</div>

		</div>
		
		<?php if ($user['Grupo']['id'] == 2 or $user['Grupo']['id'] == 3) { ?>
		<div class="col-md-6">
			
			<div class="box box-danger">
				<div class="box-header">
					<h3 class="box-title">Rejeitadas</h3>
				</div>
				<div class="box-body" style="max-height: 300px; overflow: auto;">
					<table class="table">
						<?php if (count($ocorrencias_rejeitadas) > 0) { ?>
						<thead>
							<tr class="active">
								<th colspan="2">Ocorrências</th>
							</tr>
							<tr class="active">
								<th>Nome</th>
								<th class="col-md-4">Município</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($ocorrencias_rejeitadas as $item) { ?>
							<tr>
								<td><a href="/admin/ocorrencias/edit/<?php echo $item['Ocorrencia']['id'];?>"><?php echo $item['Programa']['nome_oficial'];?></a></td>
								<td>
									<?php if (!$item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										Nacional
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['Estado']['nome'];?>
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and $item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['nome'].'/'.$item['Municipio']['Estado']['sigla'];?>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<?php } else { ?>
						<thead>
							<tr>
								<th colspan="2">
									<h4 class="text-warning">Não há Ocorrências.</h4>
								</th>
							</tr>
						</thead>
						<?php } ?>
						<?php if (count($programas_rejeitadas) > 0) { ?>
						<thead>
							<tr class="active">
								<th colspan="2">Programas</th>
							</tr>
							<tr class="active">
								<th>Nome</th>
								<th class="col-md-4">Município</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($programas_rejeitadas as $item) { ?>
							<tr>
								<td>
									<a href="/admin/programas/edit/<?php echo $item['Programa']['id'];?>">
										<?php echo $item['Programa']['nome_oficial'];?>
									</a>
								</td>
								<td>
									<?php if (!$item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										Nacional
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and !$item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['Estado']['nome'];?>
									<?php } ?>
									<?php if ($item['Programa']['estado_id'] and $item['Programa']['municipio_id']) { ?>
										<?php echo $item['Municipio']['nome'].'/'.$item['Municipio']['Estado']['sigla'];?>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<?php } else { ?>
						<thead>
							<tr>
								<th colspan="2">
									<h4 class="text-warning">Não há Programas.</h4>
								</th>
							</tr>
						</thead>
						<?php } ?>
					</table>
				</div>
			</div>

		</div>
		<?php } ?>
	</div>
	
</div>
