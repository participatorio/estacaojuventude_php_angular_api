<?php
	echo $this->Html->script('/bower_components/angular/angular.min');
	echo $this->Html->script('/bower_components/angular-classy/angular-classy.min');
	echo $this->Html->script('/bower_components/angular-resource/angular-resource.min');
	echo $this->Html->script('/bower_components/lodash/lodash.min');
	echo $this->Html->script('/bower_components/angular-google-maps/dist/angular-google-maps.min');
	// App
	echo $this->Html->script('Admin./js/Estados/app.js');
	// Controllers
	echo $this->Html->script('Admin./js/Estados/index.js');
?>

<div class="content body" ng-app="estacaoApp" ng-controller="estadoCtrl">
	<div class="box box-primary">
		<?php echo $this->Form->create('Estado'); ?>
		<?php echo $this->Form->input('id'); ?>
		<div class="box-header">
			<h3 class="box-title">Estados</h3>
			<div class="box-tools pull-right">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<div class="box-body">

			<div class="row">
				
				<div class="col-md-3">
					<?php echo $this->Form->input('sigla', array('label'=>'Sigla')); ?>
				</div>
				<div class="col-md-9">
					<?php echo $this->Form->input('nome', array('label'=>'Nome')); ?>
				</div>
				
			</div>
			
			<div class="row">
				
				<div class="col-md-4">
					<?php echo $this->Form->input('latitude', array('ng-model'=>'latitude','label'=>'Latitude')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('longitude', array('ng-model'=>'longitude','label'=>'Longitude')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('zoom', array('ng-model'=>'zoom','label'=>'Zoom')); ?>
				</div>
				
			</div>
			
		</div>
		<div style="min-height: 500px;">
			<ui-gmap-google-map center="[latitude,longitude]" zoom="zoom">
				
			</ui-gmap-google-map>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#EstadoLatitude').val(<?php echo $this->data['Estado']['latitude']; ?>);
		$('#EstadoLongitude').val(<?php echo $this->data['Estado']['longitude']; ?>);
		$('#EstadoZoom').val(<?php echo $this->data['Estado']['zoom']; ?>);
	});
</script>