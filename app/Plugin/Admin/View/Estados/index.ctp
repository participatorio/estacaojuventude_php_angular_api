<div class="content-header page-header">
	Estados
</div>
<div class="content body">

	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Lista de Estados</h3>
			<div class="box-tools pull-right">
			<div class="btn-group">
				<?php echo $this->Html->link('<i class="fa fa-plus">&nbsp;</i>Adicionar', array('action'=>'add'), array('escape'=>false,'class'=>'btn btn-sm btn-primary'));?>
			</div>
		</div>
		</div>
		<div class="box-body">

			<table class="table table-hover">

				<tr>
					<th class="col-md-1">&nbsp;</th>
					<th class="col-md-1">Sigla</th>
					<th class="col-md-10">Nome</th>
					
				</tr>

				<?php foreach($estados as $item) { ?>
				
				<tr>
					<td>
						<div class="btn-group">
							<?php echo $this->Element('index-actions', array('id'=>$item['Estado']['id'])); ?>
						</div>
					</td>
					<td><?php echo $item['Estado']['sigla'];?></td>
					<td><?php echo $item['Estado']['nome'];?></td>
				</tr>
				
				<?php } ?>

			</table>
		</div>
		<div class="box-footer">
			<div class="paginator pull-right">
				<ul class="pagination">
					<?php echo $this->Paginator->first('Primeira'); ?>
					<?php echo $this->Paginator->prev('Anterior'); ?>
				</ul>
				<?php echo $this->Paginator->numbers(); ?>
				<ul class="pagination">
					<?php echo $this->Paginator->next('Próxima'); ?>
					<?php echo $this->Paginator->last('Última'); ?>
				</ul>
			</div>
		</div>
	</div>
</div>
