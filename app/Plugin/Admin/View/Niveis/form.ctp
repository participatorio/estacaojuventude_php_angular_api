<div class="content-header">
Formulário Programa
</div>

<div class="content body">
	<div class="box box-primary">
		<?php echo $this->Form->create('Programa'); ?>
		<?php echo $this->Form->input('id'); ?>
		<div class="box-header">
			<h3 class="box-title">Programa</h3>
			<div class="box-tools pull-right">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<div class="box-body">

			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('nome_oficial', array('label'=>'Nome Oficial')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('nome_divulgacao', array('label'=>'Nome de Divulgação')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('sigla', array('label'=>'Sigla')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('duracao', array('label'=>'Duração','type'=>'text')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('idade_maxima', array('label'=>'Idade Mínima')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('idade_minima', array('label'=>'Idade Máxima')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<?php echo $this->Form->input('status_id', array('label'=>'Status')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('situacao_id', array('label'=>'Situação')); ?>
				</div>
				<div class="col-md-4">
					<?php echo $this->Form->input('nivel_id', array('label'=>'Nível')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('lei_criacao', array('label'=>'Lei de Criação')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('temporalidade_id', array('label'=>'Temporalidade')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('criterios_acesso', array('label'=>'Critérios de Acesso')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('beneficios', array('label'=>'Benefícios')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('descricao', array('label'=>'Descrição')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('objetivos', array('label'=>'Objetivos')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?php echo $this->Form->input('parceiros', array('label'=>'Parceiros')); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('observacao', array('label'=>'Observação')); ?>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<?php echo $this->Html->link('Cancelar', array('action'=>'index'), array('class'=>'btn btn-default'));?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
