<!DOCTYPE html>
<html>
	<head>
		<title>Estação Juventude</title>
		<?php echo $this->Html->script('/bower_components/jquery/dist/jquery.min'); ?>
		<?php echo $this->Html->script('/bower_components/adminlte/bootstrap/js/bootstrap.min'); ?>
		<?php echo $this->Html->script('/bower_components/adminlte/dist/js/app.min'); ?>
		
		<?php echo $this->Html->script('/js/tinymce/js/tinymce/tinymce.min.js'); ?>
		<?php echo $this->Html->script('/js/tinymce/js/tinymce/langs/pt_BR.js'); ?>
		
		<?php echo $this->Html->script('/js/datepicker/js/bootstrap-datepicker.min.js'); ?>
		<?php echo $this->Html->script('/js/datepicker/locales/bootstrap-datepicker.pt-BR.min.js'); ?>
		<?php echo $this->Html->css('/js/datepicker/css/bootstrap-datepicker3.min'); ?>
			
		<?php echo $this->Html->css('/bower_components/adminlte/bootstrap/css/bootstrap.min'); ?>
		<?php echo $this->Html->css('/bower_components/adminlte/dist/css/AdminLTE.min'); ?>
		<?php echo $this->Html->css('/bower_components/adminlte/dist/css/skins/_all-skins.min'); ?>
		<?php echo $this->Html->css('/bower_components/fontawesome/css/font-awesome.min'); ?>
		
		<style>
		
		body {
			margin-bottom: 40px !important;
		}
		
		</style>
	</head>
	<body class="skin-blue layout-top-nav">
		<header class="main-header">
			<?php echo $this->Element('top-menu'); ?>
		</header>
		<main class="content-wrapper">
			<div class="container-fluid">
				<div class="content-header">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<?php echo $this->Session->flash(); ?>
						<style>
							.alert {
								position: absolute !important;
								width: 340px !important;
								z-index: 1;
							}
						</style>
					</div>
				</div>
				</div>
				<?php echo $this->fetch('content'); ?>
			</div>
		</main>
		<footer><?php echo $this->Element('bottom-menu'); ?></footer>
	</body>
</html>

