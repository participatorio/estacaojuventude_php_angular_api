<!DOCTYPE html>
<html>
	<head>
		<title>Estação Juventude</title>
		<?php echo $this->Html->script('/bower_components/jquery/dist/jquery.min'); ?>
		<?php echo $this->Html->script('/bower_components/adminlte/bootstrap/js/bootstrap.min'); ?>
		<?php echo $this->Html->script('/bower_components/adminlte/dist/js/app.min'); ?>

		<?php echo $this->Html->css('/bower_components/adminlte/bootstrap/css/bootstrap.min'); ?>
		<?php echo $this->Html->css('/bower_components/adminlte/dist/css/AdminLTE.min'); ?>
		<?php echo $this->Html->css('/bower_components/adminlte/dist/css/skins/_all-skins.min'); ?>
		<?php echo $this->Html->css('/bower_components/fontawesome/css/font-awesome.min'); ?>
	</head>
	<body class="skin-blue layout-top-nav">
		<header class="main-header">
		</header>
		<main class="content-wrapper">
			<div class="container">
				<div class="content-header">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<?php echo $this->Session->flash(); ?>
					</div>
				</div>
				</div>
				<?php echo $this->fetch('content'); ?>
			</div>
		</main>
		
	</body>
</html>

