<?php
class Pagina extends AdminAppModel {
	
	public $useTable = 'paginas';
		
	public $hasMany = [
		'Imagem' => [
			'className' => 'Admin.Imagem',
			'foreingKey' => 'pagina_id'
		]
	];

	
}
