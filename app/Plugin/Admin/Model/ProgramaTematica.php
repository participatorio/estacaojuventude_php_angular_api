<?php
class ProgramaTematica extends AdminAppModel {
	
	public $useTable = 'programas_tematicas';
	
	public $belongsTo = [
		'Tematica',
		'Programa'	
	];

	
}
