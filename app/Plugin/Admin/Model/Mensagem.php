<?php
class Mensagem extends AdminAppModel {
	
	public $useTable = 'mensagens';
	
	public $order = [
		'Mensagem.data' => 'DESC'
	];
	
	public $belongsTo = [
		'Remetente' => [
			'className' => 'Admin.Usuario',
			'foreignKey' => 'remetente_id'
		],
		'Destinatario' => [
			'className' => 'Admin.Usuario',
			'foreignKey' => 'destinatario_id'
		],
		'Pasta'
	];
	
	public function afterFind($results, $primary = false) {
		foreach($results as $key=>$record) {
			if (isset($record['Mensagem']['data'])) {
				$data = strtotime($record['Mensagem']['data']);
				$results[$key]['Mensagem']['data'] = date('d/m/Y', $data);
			}
		}
		return $results;
	}
	
	
}
