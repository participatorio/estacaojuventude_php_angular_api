<?php
class OrgaoPrograma extends AdminAppModel {
	
	public $useTable = 'orgaos_programas';
	
	public $belongsTo = [
		'Orgao',
		'Programa'	
	];

	
}
