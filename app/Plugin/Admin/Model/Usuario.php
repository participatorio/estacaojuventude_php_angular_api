<?php
class Usuario extends AdminAppModel {
	
	public $useTable = 'usuarios';
	
	public $order = [
		'Usuario.login' => 'ASC'
	];
	
	public $belongsTo = [
		
		'Municipio' => [
			'className' => 'Admin.Municipio'
		],
		'Estado' => [
			'className' => 'Admin.Estado'
		],
		
		'Grupo' => [
			'className' => 'Admin.Grupo'
		]
	
	];
	
	
}
