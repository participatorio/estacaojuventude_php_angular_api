<?php
class Ocorrencia extends AdminAppModel {
	
	//public $order = array('Ocorrencia.data_inicial'=>'DESC');
	public $useTable = 'ocorrencias';
	public $recursive = 2;
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key=>$value) {
			if (isset($value['Ocorrencia']['inicio_inscricoes'])) {
				$data = date_create_from_format('Y-m-d', $value['Ocorrencia']['inicio_inscricoes']);
				$results[$key]['Ocorrencia']['inicio_inscricoes'] = date_format($data, 'd/m/Y');
			}
			if (isset($value['Ocorrencia']['fim_inscricoes'])) {
				$data = date_create_from_format('Y-m-d', $value['Ocorrencia']['fim_inscricoes']);
				$results[$key]['Ocorrencia']['fim_inscricoes'] = date_format($data, 'd/m/Y');
			}
		}
		return $results;
	}
	
	public function beforeSave($options = Array()) {
		if (isset($this->data['Ocorrencia']['inicio_inscricoes'])) {
			$data = date_create_from_format('d/m/Y', $this->data['Ocorrencia']['inicio_inscricoes']);
			$this->data['Ocorrencia']['inicio_inscricoes'] =  date_format($data, 'Y-m-d');
		}
		if (isset($this->data['Ocorrencia']['fim_inscricoes'])) {
			$data = date_create_from_format('d/m/Y', $this->data['Ocorrencia']['fim_inscricoes']);
			$this->data['Ocorrencia']['fim_inscricoes'] =  date_format($data, 'Y-m-d');
		}
	} 
	
	public $belongsTo = array(
		'Programa' => array(
			'className' => 'Admin.Programa'
		),
		'Municipio' => array(
			'className' => 'Admin.Municipio'
		),
		'Status' => array(
			'className' => 'Admin.Status'
		),
		'Situacao' => array(
			'className' => 'Admin.Situacao'
		)
		
	);
	
	public $hasMany = array(
		'Localizacao' => array(
			'className' => 'Admin.Localizacao',
			'foreignKey' => 'ocorrencia_id'
		)
	);

	public $validate = [
				
		'inicio_inscricoes' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		'fim_inscricoes' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'quantidade_vagas' => [
			'rule' => 'numeric',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'como_acessar' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'latitude' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'longitude' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		]

		
	];
	
}
