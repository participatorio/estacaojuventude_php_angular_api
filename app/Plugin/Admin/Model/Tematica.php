<?php
class Tematica extends AdminAppModel {
	
	public $order = array('Tematica.nome'=>'ASC');
	
	public $hasMany = [
		'ProgramaTematica' => [
			'className' => 'Admin.ProgramaTematica'
		]
	];

	
}
