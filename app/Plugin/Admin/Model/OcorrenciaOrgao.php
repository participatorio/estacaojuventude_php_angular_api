<?php
class OcorrenciaOrgao extends AdminAppModel {
	
	public $useTable = 'ocorrencias_orgaos';
	
	public $belongsTo = [
		'Orgao',
		'Ocorrencia'
	];
}
