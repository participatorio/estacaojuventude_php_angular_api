<?php
class Municipio extends AdminAppModel {
	
	public $useTable = 'municipios';
	
	public $belongsTo = array(
		'Estado' => array(
			'className' => 'Admin.Estado'
		)
	);
	
}
