<?php
class Localizacao extends AdminAppModel {
	
	public $useTable = 'localizacoes';
	
	public $belongsTo = [
		'Ocorrencia' => [
			'className' => 'Admin.Ocorrencia',
			'foreignKey' => 'ocorrencia_id'
		]
	];
	
	public $validate = [
		
		'local' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'nome_referencia' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
	];
	
	
}
