<?php
class Programa extends AdminAppModel {
	
	public $order = ['Programa.nome_divulgacao'=>'ASC'];
	public $useTable = 'programas';
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key=>$value) {
			if (isset($value['Programa']['data_inicio'])) {
				$data = date_create_from_format('Y-m-d', $value['Programa']['data_inicio']);
				$results[$key]['Programa']['data_inicio'] = date_format($data, 'd/m/Y');
			}
		}
		return $results;
	}
	
	public function beforeSave($options = Array()) {
		if (isset($this->data['Programa']['data_inicio']) && !empty($this->data['Programa']['data_inicio'])) {
			$data = date_create_from_format('d/m/Y', $this->data['Programa']['data_inicio']);
			$this->data['Programa']['data_inicio'] =  date_format($data, 'Y-m-d');
		} else {
			pr($this->data['Programa']['data_inicio']);
		}
	} 
	
	public $belongsTo = [
		'Status' => [
			'className' => 'Admin.Status'
		],
		'Situacao' => [
			'className' => 'Admin.Situacao'
		],
		'Nivel' => [
			'className' => 'Admin.Nivel'
		],
		'Temporalidade' => [
			'className' => 'Admin.Temporalidade'
		],
		'Municipio' => array(
			'className' => 'Admin.Municipio'
		),
		'Estado' => array(
			'className' => 'Admin.Estado'
		)
	];
	
	public $hasMany = [
		'ProgramasTematicas',
		'OrgaosProgramas'
	];
	
	public $validate = [
		'nome_oficial' => [
			'blank' => [
				'rule' => 'notBlank',
				'message' => 'Este campo não pode ficar em branco.',
				'last' => false
			], 
			'unique' => [
				'rule' => 'isUnique',
				'message' => 'Já existe um Programa com este mesmo nome!'
			]
		],
		'sigla' => [
			'blank' => [
				'rule' => 'notBlank',
				'message' => 'Este campo não pode ficar em branco.',
				'last' => false
			], 
			'unique' => [
				'rule' => 'isUnique',
				'message' => 'Já existe um Programa com esta mesma sigla!'
			]
		],
		
		'data_inicio' => [
			'rule' => ['date', 'dmy'],
			'allowEmpty' => true,
			'message' => 'Formato da data inválido!'	
		],

		'descricao' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		'objetivos' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'publico_alvo' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		],
		
		'criterios_acesso' => [
			'rule' => 'notBlank',
			'message' => 'Este campo não pode ficar em branco.'
		]
				
	];

	
}
