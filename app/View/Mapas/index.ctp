<style>
.angular-google-map-container { height: 700px; }
</style>

<?php echo $this->Html->script('/bower_components/angular/angular.min'); ?>
<?php echo $this->Html->script('/bower_components/lodash/lodash.min'); ?>

<?php echo $this->Html->script('/bower_components/angular-classy/angular-classy.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-resource/angular-resource.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-sanitize/angular-sanitize.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-classy/angular-classy.min'); ?>

<?php echo $this->Html->script('/bower_components/angular-google-maps/dist/angular-google-maps.min'); ?>

<?php echo $this->Html->script('app'); ?>
<?php echo $this->Html->script('dataService'); ?>


<?php echo $this->Html->script('Controllers/mapasController'); ?>
<?php echo $this->Html->script('Controllers/tematicasController'); ?>
<div class="content body">
<div style="height: 700px;" class="box box-primary" ng-app="estacaoApp" ng-controller="mapasCtrl">
	<div class="box-header">
		<div class="btn-group">
			<button class="btn btn-primary" ng-click="visoes('nacional')">Nacional</button>
			<button class="btn btn-success" ng-disabled="!estado_id" ng-click="visoes('estadual')">Estadual</button>
			<button class="btn btn-warning" ng-disabled="!municipio_id" ng-click="visoes('municipal')">Municipal</button>
			<button class="btn btn-danger" ng-click="visoes('voce')">Você</button>
		</div>
		<div class="btn-group pull-right">
			
			<form class="form-inline">
				<div class="form-group">
					<select ng-model="DS.selecteds.Estado" ng-change="changeEstadoCombo()" ng-options="item as item.Estado.nome for item in estados" class="form-control">
						<option value="">Escolha o Estado</option>
					</select>
				</div>
				<div class="form-group">
					<select ng-model="DS.selecteds.Municipio" ng-change="changeMunicipioCombo()" ng-options="item as item.Municipio.nome for item in municipios" class="form-control">
						<option value="">Escolha o Município</option>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" ng-model="ocorrencia_id" ng-options="item.Ocorrencia.id as item.Programa.nome_oficial+' - '+item.Municipio.nome+' / '+item.Municipio.Estado.sigla for item in ocorrencias">
						<option value="">Todas as Ocorrências</option>
					</select>
				</div>
			</form>
		</div>
	</div>
	
	<ui-gmap-google-map center="map.center" zoom="map.zoom" options="map.options">
		<?php // Controles do Mapa ?>
		<ui-gmap-map-control
			template="/js/Mapas/controls/tematicas.html"
			position="TOP_RIGHT"
			controller="tematicasCtrl"
			index="1">
		</ui-gmap-map-control>
		
		<ui-gmap-map-control
			template="/js/Mapas/controls/camadas.html"
			position="TOP_RIGHT"
			controller="tematicasCtrl"
			index="2">
		</ui-gmap-map-control>
		
		<ui-gmap-map-control
			ng-if="flags.showLocalizacaoCtrl==true"
			template="/js/Mapas/controls/ocorrencias.html"
			position="CENTER"
			controller="tematicasCtrl"
			index="1">
		</ui-gmap-map-control>
		
		<ui-gmap-markers
			ng-if="camadas.estados"
			models="markers.estados"
			options="'options'"
			idKey="'key'"
			click="clickEstadoMarker"
			fit="true"
			coords="'coords'"
			zoom="'zoom'"
			icon="'icon'">
		</ui-gmap-markers>
		
		<ui-gmap-markers
			ng-if="camadas.municipios"
			models="markers.municipios"
			options="'options'"
			idKey="'key'"
			click="clickMunicipioMarker"
			fit="true"
			coords="'coords'"
			zoom="'zoom'"
			icon="'icon'">
		</ui-gmap-markers>
		
		<ui-gmap-markers
			ng-if="camadas.ocorrencias"
			models="markers.localizacoes"
			options="'options'"
			idKey="'key'"
			click="clickLocalizacaoMarker"
			fit="true"
			coords="'coords'"
			zoom="'zoom'"
			icon="'icon'">
		</ui-gmap-markers>
		
	</ui-gmap-google-map>
</div>
</div>
