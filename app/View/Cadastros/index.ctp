<p>
	Texto descritivo para o cadastro dos usuários
</p>
<div class="box box-primary box-solid">
	<div class="box-header">
		<h3 class="box-title">Formulário de Cadastro</h3>
	</div>
	<?php echo $this->Form->create('Jovem'); ?>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->input('email'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->input('nome'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php echo $this->Form->input('uf', ['label'=>'UF']); ?>
			</div>
			<div class="col-md-6">
				<?php echo $this->Form->input('municipio', ['label'=>'Município']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->input('idade'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->input('sexo'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->input('etnia'); ?>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<button data-toggle="tooltip" title="Buscar dados do Facebook" class="btn btn-default"><i id="facebook_login" class="fa fa-2x text-primary fa-facebook"></i></button>
		<div class="btn-group pull-right">
			<button type="submit" class="btn btn-primary">Cadastrar</button>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>