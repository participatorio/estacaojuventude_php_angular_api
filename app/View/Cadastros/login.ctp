
<div class="row" style="margin-top: 100px;">
	<div class="col-md-6 col-md-offset-3">
		<div class="box box-primary box-solid">
			<div class="box-header">
				<h3 class="box-title">Login</h3>
			</div>
			<?php echo $this->Form->create('Jovem'); ?>
			<div class="box-body">
				<?php echo $this->Form->input('email'); ?>
				<?php echo $this->Form->input('senha'); ?>
			</div>
			<div class="box-footer  with-border">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" type="submit">Entrar</button>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
			<hr>
			<div class="box-body">
				<div>
					<button data-toggle="tooltip" title="Facebook" class="btn btn-default center-block"><i id="facebook_login" class="fa fa-2x text-primary fa-facebook"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>
