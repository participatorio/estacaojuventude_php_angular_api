<style>
.ngmap-container { height: 450px; }
</style>

<?php echo $this->Html->script('/bower_components/angular/angular.min'); ?>

<?php echo $this->Html->script('/bower_components/angular-classy/angular-classy.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-resource/angular-resource.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-sanitize/angular-sanitize.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-classy/angular-classy.min'); ?>

<?php echo $this->Html->script('/bower_components/ngmap/build/scripts/ng-map.min'); ?>

<?php echo $this->Html->script('app'); ?>
<?php echo $this->Html->script('dataService'); ?>


<?php echo $this->Html->script('Controllers/pesquisasController'); ?>

<div class="content body" ng-app="estacaoApp" ng-controller="pesquisasCtrl">
	
	<div class="page-header">
		<div class="row">
			<div class="col-md-6">
				<span>Pesquisas</span>
			</div>
			<div class="col-md-3">
				<input type="text" ng-model="campo_pesquisa" class="form-control" placeholder="Faça sua busca aqui...">
			</div>
			<div class="col-md-1">
				<button ng-click="pesquisaOcorrencias()" class="btn btn-default">Pesquise</button>
			</div>
			<div class="col-md-2">
				<div class="btn-group pull-right">
					<button ng-click="clickCamada('E')" class="btn" ng-class="{'btn-primary':!camadas.voce, 'btn-default':camadas.voce}">Navegue</button>
					<button ng-click="geoLocalization()" class="btn" ng-class="{'btn-danger':camadas.voce, 'btn-default':!camadas.voce}">Você</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="box box-success">
		<div class="box-header with-border">
			<span class="box-title">Temáticas</span>
		</div>
		<div class="box-body" ng-bind-html="tematicasHtml" compile-template>
		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-md-3">
					<select ng-options="item as item.Tematica.nome for item in tematicas" ng-model="selecionados.tematica" class="form-control">
						<option value="">Escolha uma Temática</option>
					</select>
				</div>
				<div class="col-md-9">&nbsp;</div>
			</div>
		</div>
	</div>
		
	<div class="row">
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header with-border">
					<span class="box-title">Estados</span>
				</div>
				<div class="box-body">
					<select ng-options="item as item.Estado.sigla for item in estados" ng-model="selecionados.estado" class="form-control">
						<option value="">Todos</option>
					</select>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header with-border">
					<span class="box-title">Municípios</span>
				</div>
				<div class="box-body">
					<select ng-options="item as item.Municipio.nome for item in municipios" ng-model="selecionados.municipio" class="form-control">
						<option value="">Todos</option>
					</select>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header with-border">
					<span class="box-title">Ocorrências</span>
				</div>
				<div class="box-body">
					<select ng-options="item as item.Programa.nome_oficial for item in ocorrencias" ng-model="selecionados.ocorrencia" class="form-control" name="">
						<option value="">Escolha a Ocorrência</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="page-header">
		<span>Mapa</span>
		<div class="btn-toolbar pull-right" role="toolbar">
			<div class="btn-group">
				<button ng-click="clickCamada('E')" class="btn" ng-class="{'btn-primary':camadas.estados, 'active':camadas.estados, 'btn-default':!camadas.estados}">Estados</button>
				<button ng-click="clickCamada('M')" class="btn" ng-class="{'btn-warning':camadas.municipios, 'active':camadas.municipios, 'btn-default':!camadas.municipios}">Estações</button>
				<button ng-click="clickCamada('O')" class="btn" ng-class="{'btn-danger':camadas.ocorrencias, 'active':camadas.ocorrencias, 'btn-default':!camadas.ocorrencias}">Ocorrências</button>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-body">
					<div>
						<ng-map zoom-to-include-markers="auto" center="{{mapSetup.coords}}" zoom="3" class="ngmap-container">
							<marker ng-if="camadas.voce" position="current" title="Você está aqui"></marker>
							<marker ng-if="camadas.voce" position="item in markers.proximos" on-click="clickOcorrenciaMarker(item)" position="{{item.coords}}"></marker>
							<marker ng-if="camadas.estados" ng-repeat="item in markers.estados" on-click="clickEstadoMarker(item)" position="{{item.coords}}" label="{{item.label}}" icon="{{item.icon}}"></marker>
							<marker ng-if="camadas.municipios" ng-repeat="item in markers.municipios" on-click="clickMunicipioMarker(item)" position="{{item.coords}}" label="{{item.label}}" icon="{{item.icon}}"></marker>
							<marker ng-if="camadas.ocorrencias" on-click="clickLocalizacaoMarker(item)" ng-repeat="item in markers.localizacoes" on-click="showInfoWindow(item)" position="{{item.coords}}" label="{{item.label}}" icon="{{item.icon}}"></marker>
							<info-window id="OcorrenciaInfo" position="{{infoHtml.position}}">
				        <div ng-non-bindable>
				          <h4>{{infoHtml.programa}}</h4>
				          <hr>
				          <h5>De {{infoHtml.de}} até {{infoHtml.ate}}</h5>
				          <p ng-bind-html="infoHtml.descricao"></p>
				          <button ng-click="clickLocalizacaoVerMais()" class="btn btn-primary">Ver mais...</button>
				        </div>
				      </info-window>
						</ng-map>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="viewOcorrencia">
		<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Ocorrência Municipal</h4>
	      </div>
	      <div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header">
									<h2 class="box-title">
										{{selecionados.ocorrencia.Programa.nome_divulgacao}}
									</h2>
								</div>
								<div class="box-footer">
									<div class="btn-group pull-right">
										<button ng-click="fechaOcorrenciaView();" class="btn btn-default">Voltar</button>
										<button class="btn btn-default">Imprimir</button>
										<button class="btn btn-default">Favoritos</button>
									</div>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Início das Inscrições</label>
												<p class="form-control-static">{{selecionados.ocorrencia.Ocorrencia.inicio_inscricoes|date:'dd/MM/y'}}</p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Fim das Inscrições</label>
												<p class="form-control-static">{{selecionados.ocorrencia.Ocorrencia.fim_inscricoes|date:'dd/MM/y'}}</p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Quantidade de Vagas</label>
												<p class="form-control-static">{{selecionados.ocorrencia.Ocorrencia.quantidade_vagas}}</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Benefícios Locais</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Ocorrencia.beneficios_locais"></p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Como Acessar ?</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Ocorrencia.como_acessar"></p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Órgãos</label>
												<ul>
													<li ng-repeat="item in selecionados.ocorrencia.OcorrenciaOrgao">
														<p ng-bind-html="item.Orgao.nome"></p>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="page-header">Programa</div>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Nome Oficial</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.nome_oficial"></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Nome Divulgação</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.nome_divulgacao"></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Sigla</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.sigla"></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Termporalidade</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Temporalidade.nome"></p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Órgãos</label>
												<ul>
													<li ng-repeat="item in selecionados.ocorrencia.Programa.OrgaoPrograma">
														<p ng-bind-html="item.Orgao.nome"></p>
													</li>
												</ul>
											</div>
										</div>
										<div class="col-md-6" ng-if="selecionados.ocorrencia.Programa.lei_criacao.length > 0">
											<div class="form-group">
												<label class="control-label">Lei</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.lei_criacao"></p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Descrição</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.descricao"></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Público Alvo</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.publico_alvo"></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Idade Mínima</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.idade_minima"></p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Idade Máxima</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.idade_maxima"></p>
											</div>
										</div>
									</div>
									<div class="row">	
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Critérios de Acesso</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.criterios_acesso"></p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Benefícios</label>
												<p class="form-control-static" ng-bind-html="selecionados.ocorrencia.Programa.beneficios"></p>
											</div>
										</div>
									</div>
									<div class="page-header">Localizações</div>
									<section ng-repeat="local in selecionados.ocorrencia.Localizacao">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Local</label>
													<p class="form-control-static" ng-bind-html="local.local"></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Referência</label>
													<p class="form-control-static" ng-bind-html="local.nome_referencia"></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Email</label>
													<p class="form-control-static" ng-bind-html="local.email"></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Endereço</label>
													<p class="form-control-static" ng-bind-html="local.endereco"></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Horário Funcionamento</label>
													<p class="form-control-static" ng-bind-html="local.horario_funcionamento"></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Vagas no Local</label>
													<p class="form-control-static" ng-bind-html="local.quantidade_vagas"></p>
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="box-footer">
									<div class="btn-group pull-right">
										<button ng-click="fechaOcorrenciaView();" class="btn btn-default">Voltar</button>
										<button class="btn btn-default">Imprimir</button>
										<button class="btn btn-default">Favoritos</button>
									</div>
								</div>
							</div>
						</div>
					</div>
	      </div>
	    </div>
		</div>
	</div>
	
</div>
<script src="http://maps.google.com/maps/api/js"></script>