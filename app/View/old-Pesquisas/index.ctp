<?php echo $this->Html->script('/bower_components/angular/angular.min'); ?>
<?php echo $this->Html->script('/bower_components/lodash/lodash.min'); ?>

<?php echo $this->Html->script('/bower_components/angular-classy/angular-classy.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-resource/angular-resource.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-sanitize/angular-sanitize.min'); ?>
<?php echo $this->Html->script('/bower_components/angular-classy/angular-classy.min'); ?>

<?php echo $this->Html->script('/bower_components/angular-google-maps/dist/angular-google-maps.min'); ?>

<?php echo $this->Html->script('app'); ?>
<?php echo $this->Html->script('dataService'); ?>


<?php echo $this->Html->script('Pesquisas/indexController'); ?>

<div class="content body" ng-app="estacaoApp" ng-controller="pesquisaCtrl">
	
	<div class="page-header">
		<h3>Pesquisas</h3>
	</div>
	
	<div class="row">
		
		<div class="col-md-3">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Temáticas</h3>
				</div>
				<div style="max-height: 450px; overflow: auto;">
					<ul class="list-group" style="margin: 0">
						<li class="list-group-item" style="cursor: pointer;" ng-class="{'active':DS.selecteds.tematicas_ids.length == 0}" ng-click="clickTematica(false)">Todas</li>
						<li ng-class="{'active':DS.selecteds.tematicas_ids.indexOf(item.Tematica.id)>-1}" ng-click="clickTematica(item)" ng-repeat="item in tematicas" class="list-group-item">{{item.Tematica.nome}}</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-2">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Estados</h3>
				</div>
				<div style="max-height: 450px; overflow: auto;">
					<ul class="list-group" style="margin: 0">
						<li class="list-group-item" style="cursor: pointer;" ng-class="{'active':DS.selecteds.Estado.id == item.Estado.id}" ng-click="clickEstado(false)">Todos</li>
						<li style="cursor: pointer;" ng-class="{'active':DS.selecteds.Estado.id == item.Estado.id}" ng-click="clickEstado(item)" ng-repeat="item in estados" class="list-group-item">{{item.Estado.nome}}</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-2">
			<div class="box box-warning">
				<div class="box-header">
					<h3 class="box-title">Municípios</h3>
				</div>
				<div style="max-height: 450px; overflow: auto;">
					<ul class="list-group" style="margin: 0">
						<li class="list-group-item" style="cursor: pointer;" ng-class="{'active':DS.selecteds.Municipio.id == item.Municipio.id}" ng-click="clickMunicipio(false)">Todos</li>
						<li style="cursor: pointer;" ng-class="{'active':DS.selecteds.Municipio.id == item.Municipio.id}" ng-click="clickMunicipio(item)" ng-repeat="item in municipios" class="list-group-item">{{item.Municipio.nome}}</li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="col-md-5">
			<div class="box box-danger">
				<div class="box-header">
					<h3 class="box-title">Ocorrências</h3>
				</div>
				<div style="max-height: 450px; overflow: auto;">
					<ul class="list-group" style="margin: 0">
						<li  style="cursor: pointer;" ng-class="{'active':DS.selecteds.Ocorrencia.Ocorrencia.id == item.Ocorrencia.id}" ng-click="clickOcorrencia(item)" ng-repeat="item in ocorrencias" class="list-group-item">
							<strong>{{item.Programa.nome_oficial}}</strong> - {{item.Municipio.nome}} / {{item.Municipio.Estado.sigla}}
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<a id="OcorrenciaBox"></a>
	<div ng-if="DS.selecteds.Ocorrencia" class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">
				{{DS.selecteds.Ocorrencia.Programa.nome_oficial}}<br>
				<small>{{DS.selecteds.Ocorrencia.Programa.nome_divulgacao}}</small>
			</h3>
		</div>
		<div class="box-body">
			
			<div class="row">
				<div class="col-md-4"><strong>Idade Mínima:</strong> {{DS.selecteds.Ocorrencia.Programa.idade_minima}}</div>
				<div class="col-md-4"><strong>Idade Máxima:</strong> {{DS.selecteds.Ocorrencia.Programa.idade_maxima}}</div>
				<div class="col-md-4"><strong>Duração:</strong> {{DS.selecteds.Ocorrencia.Programa.duracao}}</div>
			</div>
			<strong>Público Alvo:</strong>
			<span ng-bind-html="DS.selecteds.Ocorrencia.Programa.publico_alvo"></span><br>
			<strong>Benefícios:</strong>
			<span ng-bind-html="DS.selecteds.Ocorrencia.Programa.beneficios"></span><br>
			<strong>Acesso:</strong>
			<span ng-bind-html="DS.selecteds.Ocorrencia.Programa.criterios_acesso"></span><br>
			<strong>Objetivos:</strong>
			<span ng-bind-html="DS.selecteds.Ocorrencia.Programa.objetivos"></span><br>
			<hr>
			<strong>Benefícios Locais:</strong><span ng-bind-html="DS.selecteds.Ocorrencia.Ocorrencia.beneficios_locais"></span><br>
			<strong>Como Acessar:</strong><span ng-bind-html="DS.selecteds.Ocorrencia.Ocorrencia.como_acessar"></span><br>
			<strong>Período:</strong> {{DS.selecteds.Ocorrencia.Ocorrencia.inicio_inscricoes|date:'dd/MM/y'}} a {{DS.selecteds.Ocorrencia.Ocorrencia.fim_inscricoes|date:'dd/MM/y'}}<br>
			<strong>Total de Vagas:</strong> {{DS.selecteds.Ocorrencia.Ocorrencia.quantidade_vagas}}
			<hr>
			<h3>Locais</h3>
			<div ng-repeat="local in DS.selecteds.Ocorrencia.Localizacao">
				<h4 ng-bind-html="local.local"></h4>
				<strong>Endereço:</strong>
				<span ng-bind-html="local.endereco"></span><br>
				<strong>Bairro:</strong>
				<span ng-bind-html="local.bairro"></span><br>
				<strong>CEP:</strong>
				<span ng-bind-html="local.cep"></span>
				<div class="row">
					<div class="col-md-4"><strong>Contato:</strong> {{local.nome_referencia}}</div>
					<div class="col-md-4"><strong>Email:</strong> {{local.email}}</div>
					<div class="col-md-4"><strong>Telefone:</strong> {{local.telefone}}</div>
				</div>
				<hr>
			</div>
		</div>
		<div class="box-footer">
			<div class="btn-group pull-right">
				<button class="btn btn-default">Adicionar aos Favoritos</button>
			</div>
		</div>
	</div>
	
</div>