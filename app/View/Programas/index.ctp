<div class="content body">
	<div class="row">
		<div class="col-md-2">
			<h3>Programas</h3>
		</div>
		<div class="col-md-10">
			<div class="btn-grou pull-right">
				<?php echo $this->Paginator->numbers(); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="box box-success box-solid">
				<div class="box-header">
					<h3 class="box-title">Temáticas</h3>
				</div>
				
					<ul class="list-group">
						<?php foreach($tematicas as $item) { ?>
						<li style="cursor: pointer;" class="list-group-item">&raquo;&nbsp;<?php echo $item['TematicaAll']['nome'];?></li>
						<?php foreach($item['children'] as $subitem) { ?>
						<li style="cursor: pointer;" class="list-group-item">&raquo;&raquo;&nbsp;<?php echo $subitem['TematicaAll']['nome'];?></li>
						<?php } ?>
						<?php } ?>
					</ul>
				
			</div>
		</div>
		<div class="col-md-10">
			<?php $programaCount = 0; ?>
			<div class="row">
			<?php foreach($programas as $item) { ?>
			<?php if ($programaCount % 3 == 0) { ?>
			</div>
			<div class="row">
			<?php } ?>
			<div class="col-md-4">
				<div class="box box-primary box-solid">
					<div class="box-body">
						<h4 class="box-title">
							<strong><?php echo $item['Programa']['nome_oficial'];?></strong><br>
							<small><?php echo $item['Programa']['nome_divulgacao'];?></small>
						</h4>
						
					</div>
					<div class="box-body">
						
						<div class="box box-success">
							<div class="box-header">
								<h4 class="box-title">Critério de Acesso</h3>
							</div>
							<div class="box-body">
								<?php echo $item['Programa']['criterios_acesso'];?>
							</div>
						</div>
		
						<div class="box box-warning">
							<div class="box-header">
								<h4 class="box-title">Objetivo</h3>
							</div>
							<div class="box-body">
								<?php echo $item['Programa']['objetivos'];?>
							</div>
						</div>
		
						<div class="box box-danger">
							<div class="box-header">
								<h4 class="box-title">Público Alvo</h3>
							</div>
							<div class="box-body">
								<?php echo $item['Programa']['publico_alvo'];?>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="btn-group pull-right">
							<button class="btn btn-xs btn-default">Veja mais...</button>
						</div>
					</div>
				</div>
			</div>
			<?php $programaCount++; } ?>
		</div>
	</div>
</div>
	