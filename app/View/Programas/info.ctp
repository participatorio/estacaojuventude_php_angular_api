<div class="page-header">
	<?php echo $programa['Programa']['nome_oficial'];?>
	<small><?php echo $programa['Programa']['nome_divulgacao'];?></small>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Descrição</h3>
			</div>
			<div class="box-body">
				<?php echo $programa['Programa']['descricao'];?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">Critério de Acesso</h3>
			</div>
			<div class="box-body">
				<?php echo $programa['Programa']['criterios_acesso'];?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="box box-warning">
			<div class="box-header">
				<h3 class="box-title">Objetivo</h3>
			</div>
			<div class="box-body">
				<?php echo $programa['Programa']['objetivos'];?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="box box-danger">
			<div class="box-header">
				<h3 class="box-title">Público Alvo</h3>
			</div>
			<div class="box-body">
				<?php echo $programa['Programa']['publico_alvo'];?>
			</div>
		</div>
	</div>
</div>
