
<?php echo $this->Html->link('<i class="fa fa-pencil text-primary">&nbsp;</i>', array('action'=>'edit', $id), array('escape'=>false,'class'=>'btn btn-default'));?>
<?php echo $this->Form->postLink('<i class="fa fa-trash text-danger">&nbsp;</i>', array('action'=>'del', $id), array('confirm'=>'Tem Certeza?','escape'=>false,'class'=>'btn btn-default'));?>
