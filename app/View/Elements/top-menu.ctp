<nav class="navbar navbar-static-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php echo $this->Html->link('Estação Juventude', '/', array('class'=>'navbar-brand')); ?>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><?php echo $this->Html->link('Ocorrências Municipais',array('controller'=>'ocorrencias','action'=>'index')); ?></li>
				<li><?php echo $this->Html->link('Programas',array('controller'=>'programas','action'=>'index')); ?></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><?php echo $this->Html->link('Cadastro',array('controller'=>'cadastros','action'=>'index')); ?></li>
				<li><?php echo $this->Html->link('Login',array('controller'=>'cadastros','action'=>'login')); ?></li>
			</ul>
		</div>
	</div>
</nav>