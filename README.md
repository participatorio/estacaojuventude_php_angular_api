# Estação Juventude

Instalação

- PHP
- PostgreSQL
- Git
- NodeJS
- Bower

- Baixar o código do repositório
- Dentro do diretório [app/Config] copiar os arquivos database.php.default para database.php e core.php.default para core.php, ambos no mesmo
diretório [app/Config]
- Alterar o arquivo database.php para as informações necessárias do banco de dados Postgress do servidor
- No arquivo core.php alterar as varíaveis "Security.cipherSeed" (apenas números) aleatoriamente e "Security.salt" (números e letras) 
aleatoriamente, desta forma criando chaves de criptografias únicas para o servidor.
- Instalar o Bower - npm -g install bower
- No diretório app/webroot rodar o comando para instalar as dependências javascript - bower install

- Como a criptografia para gravação das senhas dos usúarios é diferente para cada servidor, deve-se criar um usuário no banco de dados,
tentar fazer o login pelo sistema e anotar a senha criptografada que aparece na tela e copia-la ao registro do usuário no banco de dados.
Após este procedimento, deve-se comentar ou apagar a linha "echo $this->Auth->password(xxxx)" no arquivo "app/Plugin/Admin/View/Auths/do_login.ctp".

Obs: O apache deve ser configurado para que o sistema esteja no diretorio "app/webroot".